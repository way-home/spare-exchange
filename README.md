# spare-exchange

spare exchange: wechat mini-program for second-hand presentation & sale platform project

## setup

install deps

```bash
yarn install
```

or

```bash
npm install
```

### create private config file

```bash
cp ./src/configs/configs.private.template.ts ./src/configs/configs.private.ts
```

then change the configs, which are stored in `/src/configs/configs.private.ts` [➥](/src/configs/configs.private.ts) and `/src/configs/configs.common.ts` [➥](/src/configs/configs.common.ts), as your wish

## start dev server

```bash
yarn dev:weapp
```

or

```bash
npm run dev:weapp
```

## build for release

```bash
yarn build:weapp
```

or

```bash
npm run build:weapp
```
