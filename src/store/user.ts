import Taro from '@tarojs/taro'
import globalHook, { StoreOfHook } from 'use-global-hook'

import { User, defaultUser, BasicUser } from '@/models'

export enum AuthDataStatus {
  Rejected,
  Authorized,
  NeverAuthorized,
  Pending
}

export enum LoginStatus {
  Failed,
  Authorized,
  Pending
}

export type UserDataT = {
  authData: {
    userDataAuth: AuthDataStatus
  }
  loginStatus: LoginStatus
} & User

export const initState: UserDataT = {
  authData: {
    userDataAuth: AuthDataStatus.Pending
  },
  loginStatus: LoginStatus.Pending,
  ...defaultUser
}

export type UserDataActions = {
  updateLoginStatus: (loginStatus: UserDataT['loginStatus']) => void
  updateAuthData: (authData: UserDataT['authData']) => void
  // update user data
  updateWechat: (wechat: Partial<UserDataT['wx_info']>) => void
  updateUserInfo: (userInfo: Partial<UserDataT['user_info']>) => void
  updateBackend: (backend: User | BasicUser) => void
  // batch update
  updateState: (newState: Partial<UserDataT>) => void
  setState: (newState: Partial<UserDataT>) => void
  resetState: () => void
}

let _store: any

export const useUserStore = globalHook<UserDataT, UserDataActions>(
  Taro,
  initState,
  {
    updateLoginStatus: (store, loginStatus) => {
      store.setState({
        loginStatus
      })
    },
    updateAuthData: (store, authData) => {
      const state = store.state
      store.setState({
        authData: {
          ...state.authData,
          ...authData
        }
      })
    },
    // update user data
    updateWechat: (store, wechat) => {
      const state = store.state
      store.setState({
        wx_info: {
          ...state.wx_info,
          ...wechat
        }
      })
    },
    updateUserInfo: (store, userInfo) => {
      const state = store.state
      store.setState({
        user_info: {
          ...state.user_info,
          ...userInfo
        }
      })
    },
    updateBackend: (store, user) => {
      const {
        wx_info: { id, wx_image, wx_num },
        ...rest
      } = user
      store.setState(rest)
      store.actions.updateWechat({ id, wx_image, wx_num })
    },
    // batch update
    updateState: (store, newState) => {
      store.setState(newState)
    },
    setState: (store, newState) => {
      store.setState({ ...initState, ...newState })
    },
    resetState: store => {
      store.setState(initState)
    }
  },
  s => {
    _store = s // get store
  }
)

export const userDataStore = _store as StoreOfHook<typeof useUserStore>
