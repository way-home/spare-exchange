import Taro, { Component, Config } from '@tarojs/taro'
import HomePage from '@/pages/home'

import { userDataStore, LoginStatus, AuthDataStatus } from '@/store/user'
import { clearLoginStatus, login } from '@/models/user'
import { getMyInfoAPI } from '@/models'
import { fetcher } from '@/utils/APIFetcher'
import { ResolvePromise } from '@/utils'

import './app.scss'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

class App extends Component {
  componentDidMount() {
    console.log('booting app...')
    ;(async () => {
      const {
        updateLoginStatus,
        updateWechat,
        updateAuthData,
        updateBackend
      } = userDataStore.actions
      try {
        // login
        // clear old login state
        await clearLoginStatus()
        let userInfo: ResolvePromise<ReturnType<typeof login>> | null = null
        try {
          userInfo = await login()
          updateLoginStatus(LoginStatus.Authorized)
        } catch (err) {
          console.error(err)
          updateLoginStatus(LoginStatus.Failed)
        }
        if (userInfo != null) {
          userDataStore.actions.updateAuthData({
            userDataAuth: AuthDataStatus.Authorized
          })
          updateWechat(userInfo.userInfo)
        } else {
          updateAuthData({
            userDataAuth: AuthDataStatus.NeverAuthorized
          })
        }
        // update backend userData
        if (userDataStore.state.loginStatus === LoginStatus.Authorized) {
          const myInfo = await fetcher(...getMyInfoAPI())
          updateBackend(myInfo)
          if (userInfo == null) updateWechat(myInfo.wx_info)
        }
      } catch (err) {
        console.error(err)
      }
    })().finally(() => console.log('app booted'))
  }

  componentDidShow() {
    // debug only, print storage when app shown
    // ;(async () => {
    //   const { keys } = ((await Taro.getStorageInfo()) as unknown) as {
    //     keys: string[]
    //   }
    //   console.log(`${keys.length} storages${keys.length === 0 ? '.' : ':'}`)
    //   for (const key of keys) {
    //     console.log(key, (await Taro.getStorage({ key })).data)
    //   }
    // })()
  }

  componentDidHide() {}

  componentDidCatchError() {}

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    pages: [
      // tab pages
      'pages/home/index',
      'pages/sell/index',
      'pages/stars/index',
      'pages/user/index',
      // good related
      'pages/search/index',
      'pages/goodDetail/index',
      'pages/goodForm/index',
      // user related
      'pages/userInfo/index',
      'pages/userForm/index',
      'pages/userHome/index',
      'pages/myGoods/index',
      // order related
      'pages/ongoingOrders/index',
      'pages/historicalOrders/index',
      // webview
      'pages/webview/index'
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#4d4d4d',
      selectedColor: '#ab2b2b',
      borderStyle: 'black',
      list: [
        {
          pagePath: 'pages/home/index',
          text: '首页',
          iconPath: './assets/img/tabbar/home.png',
          selectedIconPath: './assets/img/tabbar/home-active.png'
        },
        {
          pagePath: 'pages/sell/index',
          text: '卖东西',
          iconPath: './assets/img/tabbar/sell.png',
          selectedIconPath: './assets/img/tabbar/sell-active.png'
        },
        {
          pagePath: 'pages/stars/index',
          text: '收藏夹',
          iconPath: './assets/img/tabbar/star.png',
          selectedIconPath: './assets/img/tabbar/star-active.png'
        },
        {
          pagePath: 'pages/user/index',
          text: '我的',
          iconPath: './assets/img/tabbar/user.png',
          selectedIconPath: './assets/img/tabbar/user-active.png'
        }
      ]
    }
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return <HomePage />
  }
}

Taro.render(<App />, document.getElementById('app'))
