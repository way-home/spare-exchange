import Taro, { FC } from '@tarojs/taro'
import { View, Block } from '@tarojs/components'
import { AtAvatar } from 'taro-ui'

import classNames from 'classnames'

import { ViewProps } from '@tarojs/components/types/View'

import {
  BasicUser,
  defaultBasicUser,
  getUserInfoAPI,
  getUserStatAPI,
  UserStat
} from '@/models'
import useSWR from 'swr'

import { customAvatarSize, parseUrl } from '@/utils'

import configs from '@/configs'

import './index.scss'

export type UserCardSize = 'large' | 'medium' | 'small'

const defaultAvatarSize: Record<UserCardSize, number> = {
  large: 150,
  medium: 120,
  small: 90
}

export type ContentLevel = 'detailed' | 'minimal' | 'empty'

const levelRanking: Record<ContentLevel, number> = {
  empty: 0,
  minimal: 1,
  detailed: 2
}

interface UserCardProps {
  userId?: BasicUser['id']
  user?: BasicUser
  avatarSize?: number
  customStyle?: string
  contentLevel?: ContentLevel
  detailLayout?: 'vertical' | 'horizontal' | 'flow'
  size?: UserCardSize
  onClick?: ViewProps['onClick']
}

const UserCard: FC<UserCardProps> = ({
  customStyle = '',
  avatarSize: _avatarSize,
  userId,
  user: _user,
  contentLevel = 'minimal',
  detailLayout = 'horizontal',
  size = 'small',
  onClick = () => {},
  children
}) => {
  const { data: fetchedUser } = useSWR<BasicUser>(
    _user == null && userId != null && userId >= 0
      ? getUserInfoAPI(userId)
      : null
  )
  const user: BasicUser = _user || fetchedUser || defaultBasicUser
  const _userId: BasicUser['id'] | null =
    _user != null
      ? _user.id
      : userId != null
      ? userId
      : fetchedUser != null
      ? fetchedUser.id
      : null

  const { data: userStat } = useSWR<UserStat>(
    _userId != null && _userId >= 0 ? getUserStatAPI(_userId) : null
  )

  const level = levelRanking[contentLevel]
  const hasChildren = children != null
  const avatarSize = _avatarSize || defaultAvatarSize[size]
  return (
    <View
      className={classNames(
        'user-card',
        `user-card--${size}`,
        `user-card--${detailLayout}`,
        `user-card--${contentLevel}`
      )}
      style={customStyle}
      onClick={onClick}
    >
      <View className="user-card__avatar">
        <AtAvatar
          customStyle={customAvatarSize(avatarSize)}
          text={user.wx_info.nickName || undefined}
          image={
            user.wx_info.avatarUrl != null
              ? parseUrl(user.wx_info.avatarUrl)
              : undefined
          }
          circle
          className="user-card__avatar-image"
        />
      </View>
      <View className="user-card__detail">
        {level > levelRanking['empty'] && (
          <View className="user-card__info">
            <View className="user-card__info-item">
              <View className="user-card__info-label">昵称：</View>
              <View className="user-card__info-value">
                {user.wx_info.nickName}
              </View>
            </View>
            {level >= levelRanking['detailed'] && (
              <Block>
                <View className="user-card__info-item">
                  <View className="user-card__info-label">学院：</View>
                  <View className="user-card__info-value">
                    {user.user_info.college}
                  </View>
                </View>
                <View className="user-card__info-item">
                  {/* <View className="user-card__info-label">已卖出：</View> */}
                  <View className="user-card__info-value">
                    已售出{userStat != null ? userStat.sold : ''}件商品
                  </View>
                </View>
              </Block>
            )}
          </View>
        )}
        {hasChildren && children}
      </View>
    </View>
  )
}

UserCard.defaultProps = {
  onClick: () => {}
}

export default UserCard
