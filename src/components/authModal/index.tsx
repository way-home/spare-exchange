import Taro from '@tarojs/taro'
import { ButtonProps } from '@tarojs/components/types/Button'

import {
  clearLoginStatus,
  login,
  checkSession,
  getMyInfoAPI
} from '@/models/user'
import { fetcher } from '@/utils/APIFetcher'

import { UserDataActions, AuthDataStatus, LoginStatus } from '@/store'

import { ButtonConfAs, ShowModalParams } from '../modal/useModalState'

type LoginActions = Pick<UserDataActions, 'updateLoginStatus' | 'updateBackend'>

type FullLoginActions = Pick<
  UserDataActions,
  'updateWechat' | 'updateAuthData' | 'updateLoginStatus' | 'updateBackend'
>

export const loginAction = async (
  { updateLoginStatus, updateBackend }: LoginActions,
  onSuccess?: () => any,
  onFailed?: () => any
) => {
  try {
    // login
    updateLoginStatus(LoginStatus.Pending)
    await clearLoginStatus()
    await login()
    updateBackend(await fetcher(...getMyInfoAPI()))
    updateLoginStatus(LoginStatus.Authorized)
    onSuccess && onSuccess()
  } catch (err) {
    // login failed
    console.log(err)
    updateLoginStatus(LoginStatus.Failed)
    onFailed && onFailed()
    return
  }
}

export const ensureLoginAction = async (
  actions: LoginActions,
  onSuccess?: () => any,
  onFailed?: () => any
) => {
  if ((await checkSession()) == null) {
    loginAction(actions, onSuccess, onFailed)
  } else {
    onSuccess && onSuccess()
  }
}

export const loginHandler = (
  {
    updateWechat,
    updateAuthData,
    updateLoginStatus,
    updateBackend
  }: FullLoginActions,
  onSuccess?: () => any,
  onFailed?: () => any
): ButtonProps['onGetUserInfo'] => async ({ detail }) => {
  if (detail.userInfo != null) {
    updateAuthData({ userDataAuth: AuthDataStatus.Authorized })
    updateWechat(detail.userInfo)
    ensureLoginAction({ updateLoginStatus, updateBackend }, onSuccess, onFailed)
  } else {
    updateAuthData({ userDataAuth: AuthDataStatus.Rejected })
    onFailed && onFailed()
  }
}

interface ModalButtonConfig {
  wrapper?: <A extends [...any[]] | any[]>(
    func: (...args: A) => any
  ) => (...args: A) => any
  onSuccess?: () => any
  onFailed?: () => any
}

export const loginButton = (
  name: string,
  actions: FullLoginActions,
  { wrapper, onSuccess, onFailed }: ModalButtonConfig = {},
  style: string = ''
): ButtonConfAs<'getUserInfo'> => [
  name,
  (wrapper != null ? wrapper(loginHandler) : loginHandler)(
    actions,
    onSuccess,
    onFailed
  ),
  style,
  'getUserInfo'
]

export const loginModalConfig = (
  useActions: FullLoginActions,
  { wrapper, onSuccess, onFailed }: ModalButtonConfig = {}
): ShowModalParams => ({
  title: '用户信息授权',
  message:
    '为了为您提供更好的服务体验，我们需要您授权获取您的一些个人信息（包括个人头像，微信昵称等）',
  onClose: 0,
  buttons: [
    [
      '取消',
      (wrapper != null ? wrapper : _ => _)(() => {
        onFailed && onFailed()
      })
    ],
    loginButton('确认授权', useActions, { wrapper, onSuccess, onFailed })
  ]
})

export const infoModalConfig = ({
  wrapper,
  onSuccess,
  onFailed
}: ModalButtonConfig = {}): ShowModalParams => ({
  title: '完善用户信息',
  message: '\r\n您还没有完善您的个人信息，现在就去填写吗',
  onClose: 0,
  buttons: [
    [
      '等等再说',
      (wrapper != null ? wrapper : _ => _)(() => {
        onFailed && onFailed()
      })
    ],
    [
      '现在就去',
      (wrapper != null ? wrapper : _ => _)(() => {
        onSuccess && onSuccess()
        Taro.navigateTo({ url: '/pages/userForm/index' })
      })
    ]
  ]
})

export const resolveBoolean = (
  resolve: (value?: boolean | PromiseLike<boolean>) => void
) => ({
  onSuccess: () => resolve(true),
  onFailed: () => resolve(false)
})
