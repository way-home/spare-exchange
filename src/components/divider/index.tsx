import Taro, { FC } from '@tarojs/taro'
import { View } from '@tarojs/components'

import classnames from 'classnames'

import './index.scss'

interface DividerProps {
  height?: number
  top?: boolean
  bottom?: boolean
}

const Divider: FC<DividerProps> = ({ top = false, bottom, height = false }) => (
  <View
    className={classnames('divider', {
      'divider--top-border': top,
      'divider--bottom-border': bottom
    })}
    style={`${height != null ? `${height}rpx` : ''}`}
  ></View>
)

export default Divider
