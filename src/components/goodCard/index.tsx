import Taro, { FC } from '@tarojs/taro'
import {
  View,
  Image,
  Text,
  Block,
  Swiper,
  SwiperItem
} from '@tarojs/components'
import { ViewProps } from '@tarojs/components/types/View'

import './index.scss'

import { Good, defaultGood, isBook } from '@/models'

import classnames from 'classnames'

import { string2currency, num2discount } from '@/utils'

export type ContentLevel = 'detailed' | 'simple' | 'minimal'

const levelRanking: Record<ContentLevel, number> = {
  minimal: 0,
  simple: 1,
  detailed: 2
}

interface GoodCardProps {
  good: Good
  showPrice?: boolean
  orientation?: 'vertical' | 'horizontal'
  contentLevel?: 'detailed' | 'simple' | 'minimal'
  onClick?: ViewProps['onClick']
}

const GoodCard: FC<GoodCardProps> = ({
  good,
  children,
  showPrice = true,
  orientation = 'horizontal',
  contentLevel = 'simple',
  onClick = () => {}
}) => {
  const {
    name,
    category,
    description,
    images,
    original_price,
    current_price
  } = good
  const imageUrls = images.map(img => img.image)
  const discount =
    original_price != null
      ? parseFloat(current_price) / parseFloat(original_price)
      : null
  const level = levelRanking[contentLevel]
  const detailed = contentLevel === 'detailed'
  const hasChildren = children != null
  const imgContainerCls = classnames('good-card__image-container', {
    'good-card__image-container--not-available': images.length === 0
  })

  return (
    <View
      className={classnames(
        'good-card',
        `good-card--${orientation}`,
        `good-card--${contentLevel}`
      )}
      onClick={onClick}
    >
      {/* good-card__image */}
      {detailed ? (
        // detailed
        images.length !== 0 ? (
          // has images
          <Swiper
            className={imgContainerCls}
            indicatorDots
            autoplay
            circular
            interval={4500}
            duration={500}
          >
            {imageUrls.map(img => (
              <SwiperItem key={img} className="good-card__image">
                <Image
                  className="good-card__image"
                  src={img}
                  mode="aspectFill"
                  onClick={() => {
                    Taro.previewImage({
                      current: img,
                      urls: imageUrls
                    })
                  }}
                />
              </SwiperItem>
            ))}
          </Swiper>
        ) : (
          // no image
          <View className={imgContainerCls}>
            <Text style="color: #aaa">没有图片</Text>
          </View>
        )
      ) : (
        // not detailed
        <View className={imgContainerCls}>
          {/* only show the first image */}
          {images.length !== 0 && (
            <Image
              className="good-card__image"
              src={imageUrls[0]}
              mode="aspectFill"
            />
          )}
        </View>
      )}
      <View className="good-card__detail">
        <View className="good-card__name">{name}</View>
        {level > levelRanking['minimal'] && (
          <Block>
            {detailed ? (
              <View className="good-card__category">
                {category.description}
              </View>
            ) : (
              <View className="good-card__info">
                <View className="good-card__info-label">类别：</View>
                <View className="good-card__info-value">
                  {category.description}
                </View>
              </View>
            )}
            {isBook(good) && detailed && (
              <Block>
                <View className="good-card__info-header">商品参数</View>
                <View className="good-card__info">
                  <View className="good-card__info-label">ISBN：</View>
                  <View className="good-card__info-value">
                    {good.isbn != null ? good.isbn : ''}
                  </View>
                </View>
                <View className="good-card__info">
                  <View className="good-card__info-label">作者：</View>
                  <View className="good-card__info-value">
                    {good.author != null ? good.author : ''}
                  </View>
                </View>
                <View className="good-card__info">
                  <View className="good-card__info-label">出版社：</View>
                  <View className="good-card__info-value">
                    {good.press != null ? good.press : ''}
                  </View>
                </View>
              </Block>
            )}
            <View className="good-card__description">
              <View className="good-card__description-label">详情：</View>
              <View className="good-card__description-content">
                {level >= levelRanking['detailed'] ? (
                  <Text>{description}</Text>
                ) : (
                  description
                )}
              </View>
            </View>
          </Block>
        )}
        <View
          className={classnames('good-card__price', {
            'good-card__price--hide': !showPrice
          })}
        >
          <View className="good-card__current-price">
            {string2currency(current_price)}
          </View>
          <View className="good-card__original-price">
            {original_price != null && current_price !== original_price
              ? string2currency(original_price)
              : ''}
          </View>
          <View className="good-card__discount">
            {discount != null && discount < 1 ? num2discount(discount) : ''}
          </View>
        </View>
        {hasChildren && children}
      </View>
    </View>
  )
}

GoodCard.defaultProps = {
  good: defaultGood,
  onClick: _ => {}
}

export default GoodCard
