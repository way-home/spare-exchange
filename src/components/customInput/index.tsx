import Taro, { FC } from '@tarojs/taro'
import { View, Text, Label } from '@tarojs/components'

import classnames from 'classnames'

import './index.scss'

interface CustomInputProps {
  label: string
  required?: boolean
  labelPosition?: 'top' | 'middle' | 'bottom'
  iconPosition?: 'top' | 'middle' | 'bottom'
  labelStyle?: string
  iconStyle?: string
  disabled?: boolean
  error?: boolean
  placeholder?: string
  placeholderActive?: boolean
  onClick?: () => void
}

const CustomInput: FC<CustomInputProps> = ({
  children,
  label,
  required = false,
  labelPosition = 'middle',
  iconPosition = 'middle',
  labelStyle,
  iconStyle,
  disabled,
  error,
  placeholder,
  placeholderActive,
  onClick
}) => {
  const noChildren = children == null
  const containerCls = classnames('at-input__container', {
    'at-input--error': error,
    'at-input--disabled': disabled
  })
  const labelCls = classnames('at-input__title', 'custom-input__title', {
    'custom-input__title--top': labelPosition === 'top',
    'custom-input__title--bottom': labelPosition === 'bottom',
    'at-input__title--required': required
  })
  const iconCls = classnames('at-input__icon', 'custom-input__icon', {
    'custom-input__icon--top': iconPosition === 'top',
    'custom-input__icon--bottom': iconPosition === 'bottom'
  })
  return (
    <View className="at-input custom-input">
      <View className={containerCls}>
        <Label className={labelCls} style={labelStyle}>
          {label}
        </Label>
        <View
          className="at-input__input"
          onClick={onClick ? onClick : () => {}}
        >
          {placeholder && (placeholderActive || noChildren) && (
            <Text className="custom-input__placeholder">{placeholder}</Text>
          )}
          {!(placeholder && placeholderActive) && children}
        </View>
        {error && (
          <View className={iconCls} style={iconStyle}>
            <Text className="at-icon at-icon-alert-circle at-input__icon-alert"></Text>
          </View>
        )}
      </View>
    </View>
  )
}

export default CustomInput
