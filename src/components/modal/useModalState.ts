import Taro, { useState, useCallback, showModal } from '@tarojs/taro'
import { ButtonProps } from '@tarojs/components/types/Button'

export type CommonHandler = () => any

// [name, onClick, style?, openType?]
export type ButtonConf =
  | [string, ButtonProps['onClick']]
  | [string, ButtonProps['onClick'], string]
  | [string, ButtonProps['onGetUserInfo'], string, 'getUserInfo']
  | [string, ButtonProps['onContact'], string, 'contact']
  | [
      string,
      CommonHandler,
      string,
      Exclude<ButtonProps['openType'], 'contact' | 'getUserInfo'>
    ]

export type ButtonConfAs<T extends ButtonProps['openType']> = Extract<
  ButtonConf,
  [any, any, any, T]
>

export type Handler = ButtonConf[1]

export interface ModalState {
  show: boolean
  message: string
  title: string
  onClose: Handler
  buttons: ButtonConf[]
}

export interface ModalAction {
  setShow: (_: boolean) => void
  setMessage: (_: string) => void
  setTitle: (_: string) => void
  setOnClose: (_: Handler) => void
  setButtons: (_: ButtonConf[]) => void
}

export interface ShowModalParams
  extends Partial<
    Omit<ModalState, 'show' | 'onClose'> & {
      // allow user to use number to specify
      // a handler that is as same as the button with such index
      onClose: ModalState['onClose'] | number
    }
  > {
  onEnter?: CommonHandler
  onLeave?: CommonHandler
}

export const defaultModalState: ModalState = {
  show: false,
  message: '',
  title: '提示',
  onClose: () => {},
  buttons: [
    ['取消', () => {}],
    ['确定', () => {}]
  ]
}

export const useShowModal = (
  actions: ModalAction,
  fallbackState: Partial<Omit<ModalState, 'show'>> = {}
) => {
  const defaultState: ModalState = {
    ...defaultModalState,
    ...fallbackState
  }
  return useCallback(
    (params: ShowModalParams) => {
      const { onEnter, onLeave, ...state } = params
      const newState = { ...defaultState, ...state }
      const warpHandler = <A extends [any] | any[]>(
        handler?: (...args: A) => any
      ): ((...args: A) => any) => (...args: A) => {
        const res = handler != null ? handler(...args) : undefined
        onLeave && onLeave()
        actions.setShow(false)
        return res
      }
      actions.setTitle(newState.title)
      actions.setMessage(newState.message)
      actions.setButtons(
        newState.buttons.map(btn => {
          const warpedBtn = [...btn] as typeof btn
          warpedBtn[1] = warpHandler(warpedBtn[1])
          return warpedBtn
        })
      )
      actions.setOnClose(() =>
        warpHandler(
          typeof newState.onClose === 'number'
            ? newState.buttons[newState.onClose][1]
            : newState.onClose
        )
      )
      onEnter && onEnter()
      actions.setShow(true)
    },
    [actions, fallbackState]
  )
}

export const useModalState = (
  initShow: boolean = false
): [ModalState, ModalAction] => {
  const [show, setShow] = useState<boolean>(initShow)
  const [message, setMessage] = useState<string>('')
  const [title, setTitle] = useState<string>('')
  const [buttons, setButtons] = useState<ButtonConf[]>([])

  // bypass useState to set a state to a function
  const [onClose, setOnClose] = useState<Handler>(() => () => {})

  return [
    {
      show,
      message,
      title,
      onClose,
      buttons
    },
    {
      setShow,
      setMessage,
      setTitle,
      setOnClose,
      setButtons
    }
  ]
}
