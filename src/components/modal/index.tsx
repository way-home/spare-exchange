import Taro, { FC } from '@tarojs/taro'
import { Text, Button } from '@tarojs/components'

import { AtModal, AtModalHeader, AtModalContent, AtModalAction } from 'taro-ui'

import { ModalState, defaultModalState } from './useModalState'

interface ModalProps {
  state: ModalState
}

const Modal: FC<ModalProps> = ({ state }) => {
  const { show, onClose, title, message, buttons } = {
    ...defaultModalState,
    ...state
  }
  return (
    <AtModal isOpened={show} onClose={onClose}>
      <AtModalHeader>{title}</AtModalHeader>
      <AtModalContent>
        <Text>{message}</Text>
      </AtModalContent>
      <AtModalAction>
        {buttons.map(btn => {
          const [name, onClick, style, openType] = btn
          const isClickHandled =
            onClick != null &&
            !(['contact', 'getUserInfo'] as typeof openType[]).includes(
              openType
            )
          return (
            <Button
              key={name}
              style={style ? style : ''}
              openType={openType}
              onClick={isClickHandled ? onClick : undefined}
              onContact={btn[3] === 'contact' ? btn[1] : undefined}
              onGetUserInfo={btn[3] === 'getUserInfo' ? btn[1] : undefined}
            >
              {name}
            </Button>
          )
        })}
      </AtModalAction>
    </AtModal>
  )
}

export default Modal
