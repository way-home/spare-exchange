import Taro, { FC } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import classNames from 'classnames'

import {
  defaultOrder,
  Good,
  Order,
  orderStatus2String,
  isCancelled,
  getGoodAPI,
  OrderStatus
} from '@/models'
import useSWR from 'swr'

import format from 'date-fns/format'

import './index.scss'

type TimeType = 'start' | 'done' | 'good'

interface OrderCardProps {
  order: Order
  customStyle?: string
  showStatus?: boolean
  timeType?: TimeType
}

const OrderCard: FC<OrderCardProps> = ({
  customStyle = '',
  showStatus = false,
  order: _order,
  timeType = 'start',
  children
}) => {
  const { data: good } = useSWR<Good>(
    _order != null ? getGoodAPI(_order.good) : null
  )
  const order = _order || defaultOrder
  const hasImage = good != null && good.images.length
  const hasChildren = children != null
  const time = {
    start: order.start_time,
    done: null,
    good: good != null ? good.publish_time : null
  }[timeType]
  return (
    <View className="order-card" style={customStyle}>
      <View
        className={classNames('order-card__image-container', {
          'order-card__image-container--not-available': !hasImage
        })}
      >
        {hasImage && (
          <Image
            className="order-card__image"
            src={good!.images[0].image}
            mode="aspectFill"
          />
        )}
      </View>
      <View className="order-card__detail">
        <View className="order-card__info">
          <View className="order-card__name">{good && good.name}</View>
          <View className="order-card__time">
            {time != null ? format(time, 'yyyy-MM-dd') : '????-??-??'}
          </View>
        </View>
        {showStatus && (
          <View className="order-card__status">
            {order.status == OrderStatus.CancelledByBuyer
              ? '订单已取消'
              : isCancelled(order.status)
              ? '订单已失效'
              : orderStatus2String(order.status)}
          </View>
        )}
        {hasChildren && children}
      </View>
    </View>
  )
}

OrderCard.defaultProps = {}

export default OrderCard
