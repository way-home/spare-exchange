import {
  fetcher,
  APIConfig,
  basicUrlGen,
  basicParamGen
} from '@/utils/APIFetcher'
import Taro from '@tarojs/taro'
import fetch, { defaultCookieGen } from '@/utils/fetch'
import { PartialNullable, parseUrl } from '@/utils'

export type WxInfo = Taro.UserInfo & {
  wx_image: string
  wx_num: string
}

export type UserInfo = {
  number: string
  name: string
  email: string
  phone: string
  college: string
}

export interface BasicUser {
  id: number // 用户id
  wx_info: {
    id: number
  } & PartialNullable<WxInfo>
  user_info: {
    id: number
  } & PartialNullable<UserInfo>
}

export interface User extends BasicUser {
  username: string // 用户名
  admin: boolean // 是否为管理员
}

export interface UserStat {
  sold: number // 已卖出商品总数
}

export const defaultBasicUser: BasicUser = {
  id: -1,
  user_info: {
    id: -1
  },
  wx_info: {
    id: -1
  }
}

export const defaultUser: User = {
  ...defaultBasicUser,
  username: 'username',
  admin: false
}

export const parseBasicUser = (data: any): BasicUser => {
  const { user_info, wx_info, ...rest } = data
  return {
    ...rest,
    user_info: user_info != null ? user_info : {},
    wx_info: wx_info != null ? wx_info : {}
  }
}

export const parseUser = (data: any): User => {
  const { user_info, wx_info, ...rest } = data
  const { wx_image = null, ...rest_wx_info } = wx_info != null ? wx_info : {}
  const post_wx_image = wx_image != null ? parseUrl(wx_image) : null
  return {
    user_info: user_info != null ? user_info : {},
    wx_info: {
      wx_image: post_wx_image,
      ...rest_wx_info
    },
    ...rest
  }
}

export const parseUserStat = (data: any): UserStat => data

export const parseUserInfo = (data: any): BasicUser['user_info'] => data

export interface EssentialInfoOnly {
  wx_info: PartialNullable<Pick<WxInfo, 'wx_image' | 'wx_num'>>
  user_info: PartialNullable<
    Pick<UserInfo, 'number' | 'name' | 'college' | 'email' | 'phone'>
  >
}

export const hasCompleteInfo = (essentialInfo: EssentialInfoOnly): boolean => {
  const {
    user_info: { number, name, college }
  } = essentialInfo
  return (
    [number, name, college].every(info => info != null && info !== '') &&
    hasContactInfo(essentialInfo)
  )
}

export interface ContactInfoOnly {
  wx_info: PartialNullable<Pick<WxInfo, 'wx_image' | 'wx_num'>>
  user_info: PartialNullable<Pick<UserInfo, 'email' | 'phone'>>
}

export const hasContactInfo = (contactInfo: ContactInfoOnly): boolean => {
  const {
    wx_info: { wx_image, wx_num },
    user_info: { email, phone }
  } = contactInfo
  return [wx_image, wx_num, email, phone].some(
    info => info != null && info !== ''
  )
}

export const personalDataUrl = '/user_management/personal_data/' as const
export const userDataUrl = '/user_management/user_read_only/' as const
export const userStatUrl = '/spareExchange/user-statistics/' as const

export const getMyInfoPost = ({ data: { data } }) => parseUser(data)
export const getMyInfoAPI = (): APIConfig<User> => [
  'GET',
  personalDataUrl,
  basicUrlGen,
  basicParamGen,
  getMyInfoPost
]

export const getUserInfoUrlGen = (url: string, id: number) => `${url}${id}/`
export const getUserInfoPost = ({ data }) => parseBasicUser(data)
export const getUserInfoAPI = (id: BasicUser['id']): APIConfig<BasicUser> => [
  'GET',
  userDataUrl,
  getUserInfoUrlGen,
  basicParamGen,
  getUserInfoPost,
  id
]

export const getUserStatParamsGen = (user_id: BasicUser['id']) => ({ user_id })
export const getUserStatPost = ({ data }) => parseUserStat(data)
export const getUserStatAPI = (
  user_id: BasicUser['id']
): APIConfig<UserStat> => [
  'GET',
  userStatUrl,
  basicUrlGen,
  getUserStatParamsGen,
  getUserStatPost,
  user_id
]

export const updateMyInfo = (info: PartialNullable<UserInfo & WxInfo>) =>
  fetch
    .post(personalDataUrl, info)
    .then(({ data: { data } }) => parseUser(data))

export const updateWxImage = (localUrl: Exclude<WxInfo['wx_image'], null>) =>
  Taro.uploadFile({
    url: parseUrl(personalDataUrl),
    filePath: localUrl,
    name: 'wx_image',
    header: {
      cookie: defaultCookieGen(),
      'X-CSRFToken': Taro.getStorageSync('csrftoken')
    }
  }).then(({ data }) => parseUser(JSON.parse(data).data))

export const backendLogin = async (
  code?: Taro.login.SuccessCallbackResult['code'],
  userInfo?: Taro.getUserInfo.SuccessCallbackResult | null
) => {
  const { data } = await fetch.post(
    '/user_management/loginWx/',
    {
      code,
      source: 'esjy',
      raw: userInfo != null ? userInfo : undefined
    },
    {
      headers: {
        'Content-Type': 'application/json'
      }
    }
  )
  return data.session_id
}

export const clearLoginStatus = () =>
  Promise.all(
    ['session_id', 'csrftoken'].map(key => Taro.removeStorage({ key }))
  )

export const checkSession = async (): Promise<
  BasicUser['user_info'] | null
> => {
  try {
    await Promise.all([
      Taro.getStorage({ key: 'session_id' }),
      Taro.checkSession()
    ])
    return await fetcher(...getMyInfoAPI())
  } catch (err) {
    console.error(err)
    return null
  }
}

export const login = async () => {
  // wx login
  const { code } = await Taro.login()
  // wx userInfo
  let allUserInfo: Taro.getUserInfo.SuccessCallbackResult | null = null
  let userInfoAuthorized: boolean = false
  try {
    if (!(await Taro.getSetting()).authSetting['scope.userInfo']) {
      await Taro.authorize({ scope: 'scope.userInfo' })
    }
    userInfoAuthorized = true
  } catch (err) {
    console.error(err)
  }
  if (userInfoAuthorized)
    allUserInfo = await Taro.getUserInfo({ withCredentials: true })
  // backend login
  const session_id = await backendLogin(code, allUserInfo)
  await Taro.setStorage({
    key: 'session_id',
    data: session_id
  })
  return allUserInfo
}
