import { Good, parseGood, defaultGood } from './good'
import { User } from './user'
import fetch from '@/utils/fetch'
import { APIConfig, basicUrlGen, basicParamGen } from '@/utils/APIFetcher'
import { filterProps } from '@/utils'

export interface GoodStar {
  id: number
  good: Good
  add_time: Date
}

export const defaultGoodStar = {
  id: 0,
  good: defaultGood,
  add_time: new Date()
}

export const parseGoodStar = (obj: any): GoodStar => {
  return {
    id: obj.id,
    good: parseGood(obj.good_info),
    add_time: new Date(obj.add_time)
  }
}

export const goodStarUrl = '/spareExchange/good-stars/' as const

export const getGoodStarUrlGen = (url: string, id: GoodStar['id']) =>
  `${url}${id}/`
export const getGoodStarPost = ({ data }) => parseGoodStar(data)
export const getGoodStarAPI = (id: number): APIConfig<GoodStar> => [
  'GET',
  goodStarUrl,
  getGoodStarUrlGen,
  basicParamGen,
  getGoodStarPost,
  id
]

type GetGoodStarsParams = {
  user?: User['id']
  status?: 'valid' | 'invalid'
}

export const getGoodStarsParamsGen = (
  user: User['id'],
  status: 'valid' | 'invalid'
) => filterProps({ user, status }, (k, v) => v !== undefined)
export const getGoodStarsPost = ({ data }) => (data as any[]).map(parseGoodStar)
export const getGoodStarsAPI = ({
  user,
  status
}: GetGoodStarsParams = {}): APIConfig<GoodStar[]> => [
  'GET',
  goodStarUrl,
  basicUrlGen,
  getGoodStarsParamsGen,
  getGoodStarsPost,
  user,
  status
]

export const createGoodStar = (good: Good['id']) =>
  fetch.post(`${goodStarUrl}`, { good }).then(({ data }) => parseGoodStar(data))

export const deleteGoodStar = (id: GoodStar['id']) =>
  fetch.delete(`${goodStarUrl}${id}/`)
