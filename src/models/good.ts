import {
  Image,
  parseImage,
  Category,
  defaultCategory,
  User,
  parseCategory
} from '@/models'
import { basicUrlGen, basicParamGen, APIConfig } from '@/utils/APIFetcher'
import fetch from '@/utils/fetch'
import {
  PartialExclude,
  PartialNullable,
  PartialNullableExclude,
  UnionToIntersection,
  filterProps
} from '@/utils'

export const enum GoodStatus {
  Unknown = 0,
  ForSell = 1,
  Sold = 2,
  NotAvailable = 3
}

const GoodStatusString = {
  [GoodStatus.Unknown]: '未知',
  [GoodStatus.ForSell]: '待售',
  [GoodStatus.NotAvailable]: '下架'
}

export const goodStatus2String = (status: GoodStatus): string => {
  const str = GoodStatusString[status]
  return str != null ? str : '未知状态'
}

export interface BasicGood {
  id: number
  owner: User['id']
  category: Category
  name: string
  publish_time: Date
  current_price: string
  original_price?: string
  description: string
  images: Image[] // list of url
  click_num: number
  status: GoodStatus
}

export interface Book extends BasicGood {
  author: string
  isbn: string
  press: string
}

export type Good = BasicGood | Book

export const isBook = (good: Good): good is Book =>
  ['author', 'isbn', 'press'].some(k => good[k] != null)

export const defaultGood: Good = {
  owner: 0,
  category: defaultCategory,
  id: 0,
  name: 'name',
  publish_time: new Date(),
  current_price: '0',
  description: 'good',
  images: [],
  click_num: 0,
  status: GoodStatus.Unknown
}

export const parseGood = (obj: any): Good => {
  const {
    status,
    publish_time,
    category,
    category_info,
    book,
    images_info,
    ...rest
  } = obj
  let good: Good = rest
  if (images_info) good.images = (images_info as any[]).map(parseImage)
  if (status) good.status = status
  if (publish_time) good.publish_time = new Date(publish_time)
  if (category_info) good.category = parseCategory(category_info)
  if (book) good = { ...book, ...good }
  return good
}

export const good2Backend = (goodInfo: PartialNullable<Good>): any => {
  const {
    category,
    author,
    isbn,
    press,
    images,
    ...rest
  } = goodInfo as PartialNullable<UnionToIntersection<Good>>
  const bookExtra = { author, isbn, press }

  let payload: Record<string, any> = rest
  if (category !== undefined)
    payload.category = category !== null ? category.id : category
  if (images != null) payload.images = images.map(({ id }) => id)
  // add extra field for book
  for (const [k, v] of Object.entries(bookExtra)) {
    if (v !== undefined) {
      if (payload.book == null) payload.book = {}
      payload.book[k] = v
    }
  }

  return payload
}

export const goodUrl = '/spareExchange/goods/' as const

type GetGoodsParams = {
  owner?: User['id']
}

export const getGoodsParamsGen = (owner: User['id']) =>
  filterProps({ owner }, (k, p) => p !== undefined)
export const getGoodsPost = ({ data }) => (data as any[]).map(parseGood)
export const getGoodsAPI = ({
  owner
}: GetGoodsParams = {}): APIConfig<Good[]> => [
  'GET',
  goodUrl,
  basicUrlGen,
  getGoodsParamsGen,
  getGoodsPost,
  owner
]

export const getGoodUrlGen = (url: string, id: number) => `${url}${id}/`
export const getGoodPost = ({ data }) => parseGood(data)
export const getGoodAPI = (id: number): APIConfig<Good> => [
  'GET',
  goodUrl,
  getGoodUrlGen,
  basicParamGen,
  getGoodPost,
  id
]

export const getUserGoodsPost = ({ data }) => data.map(parseGood)
export const getUserGoodsParamsGen = (user: number) => ({ user })
export const getUserGoodsAPI = (user: number): APIConfig<Good[]> => [
  'GET',
  goodUrl,
  basicUrlGen,
  getUserGoodsParamsGen,
  getUserGoodsPost,
  user
]

export const createGood = (goodInfo: Partial<Good>): Promise<Good> =>
  fetch
    .post(goodUrl, { book: null, ...good2Backend(goodInfo) })
    .then(res => parseGood(res.data))

export const refreshGood = (
  goodInfo: PartialExclude<Good, 'id'>
): Promise<Good> => {
  const { id, ...info } = goodInfo
  return fetch
    .put(`${goodUrl}${id}/`, good2Backend(info))
    .then(res => parseGood(res.data))
}

export const updateGood = (
  goodInfo: PartialNullableExclude<Good, 'id'>
): Promise<Good> => {
  const { id, ...info } = goodInfo
  return fetch
    .patch(`${goodUrl}${id}/`, good2Backend(info))
    .then(res => parseGood(res.data))
}
