import fetch from '@/utils/fetch'

export interface BookData {
  title: string //标题
  author: string //作者
  publisher: string //出版社
  price: string //价格
}

export const parseBookData = (data: any): BookData => data

const queryISBNUrl = '/spareExchange/isbn-query/' as const

export const queryISBN = (isbn: string): Promise<BookData> =>
  fetch.post(queryISBNUrl, { isbn }).then(({ data }) => parseBookData(data))
