import { Good } from '@/models'
import { filterProps } from '@/utils'
import { APIConfig, basicUrlGen, basicParamGen } from '@/utils/APIFetcher'
import fetch from '@/utils/fetch'

export interface Order {
  id: number
  good: number // 物品id
  seller: number // 卖家id
  buyer: number // 买家id
  start_time: Date
  status: OrderStatus // 订单状态
}

export enum OrderStatus {
  Unknown = 0,
  OnGoing = 1,
  Success = 2,
  CancelledByBuyer = 3,
  CancelledBySeller = 4,
  CancelledByGood = 5
}

export const defaultOrder: Order = {
  id: 0,
  good: 0,
  seller: 0,
  buyer: 0,
  start_time: new Date(),
  status: OrderStatus.Unknown
}

export const OrderStatusString = {
  [OrderStatus.Unknown]: '未知状态',
  [OrderStatus.OnGoing]: '进行中',
  [OrderStatus.Success]: '已成交',
  [OrderStatus.CancelledByBuyer]: '由买家取消',
  [OrderStatus.CancelledBySeller]: '由卖家取消',
  [OrderStatus.CancelledByGood]: '由物品原因取消'
}

export const isCancelled = (status: OrderStatus) => {
  return [
    OrderStatus.CancelledByBuyer,
    OrderStatus.CancelledByGood,
    OrderStatus.CancelledBySeller
  ].some(s => s === status)
}

export const orderStatus2String = (status: OrderStatus): string => {
  return OrderStatusString[status] || '未知状态'
}

export const parseOrder = (obj: any): Order => {
  return {
    ...(obj as Order),
    start_time: new Date(obj.start_time)
  }
}

export const getOrderUrl = '/spareExchange/order/' as const
export const manageOrderUrl = '/spareExchange/order-management/' as const

export const getOrderUrlGen = (url, id: Order['id']) => `${url}${id}/`
export const getOrderPost = ({ data }) => parseOrder(data)
export const getOrderAPI = (id: Order['id']): APIConfig<Order> => [
  'GET',
  getOrderUrl,
  getOrderUrlGen,
  basicParamGen,
  getOrderPost,
  id
]

type getOrdersParams = {
  good?: Good['id']
}

export const getOrdersParamsGen = (good_id: Good['id']) =>
  filterProps(
    {
      good_id
    },
    (k, v) => v != null
  )
export const getOrdersPost = ({ data }) => (data as any[]).map(parseOrder)
export const getOrdersAPI = ({
  good
}: getOrdersParams = {}): APIConfig<Order[]> => [
  'GET',
  getOrderUrl,
  basicUrlGen,
  getOrdersParamsGen,
  getOrdersPost,
  good
]

enum OrderOperation {
  Confirm = 0, // 确认
  Cancel = 1 // 取消
}

export const createOrder = (good: Good['id']) =>
  fetch.post(getOrderUrl, { good }).then(({ data }) => parseOrder(data))

export const confirmOrder = (id: Order['id']) =>
  fetch
    .post(manageOrderUrl, {
      id,
      op: OrderOperation.Confirm
    })
    .then(({ data }) => parseOrder(data))

export const cancelOrder = (id: Order['id']) =>
  fetch
    .post(manageOrderUrl, {
      id,
      op: OrderOperation.Cancel
    })
    .then(({ data }) => parseOrder(data))
