import Taro from '@tarojs/taro'

import { defaultCookieGen } from '@/utils/fetch'
import { parseUrl } from '@/utils'

export interface Image {
  id: number
  image: string // url
}

export const imageUrl = '/spareExchange/images/' as const

export const parseImage = (obj: any): Image => {
  const { image, ...rest } = obj
  let img = { ...rest }
  if (image != null) img.image = image
  return img
}

export const createImage = (localUrl: Image['image']) =>
  Taro.uploadFile({
    url: parseUrl(imageUrl),
    filePath: localUrl,
    name: 'image',
    header: {
      cookie: defaultCookieGen(),
      'X-CSRFToken': Taro.getStorageSync('csrftoken')
    }
  }).then(({ data }) => parseImage(JSON.parse(data)))
