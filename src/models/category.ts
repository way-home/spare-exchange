import { APIConfig, basicUrlGen, basicParamGen } from '@/utils/APIFetcher'

export interface Category {
  id: number
  parent: number | null
  description: string
}

export const defaultCategory = {
  id: 0,
  parent: null,
  description: 'category'
}

export const parseCategory = (obj: any): Category => {
  return {
    id: obj.id!,
    parent: obj.parent != null ? obj.parent : null,
    description: obj.description
  }
}

export const categoryUrl = '/spareExchange/category/' as const

export const getCategoryUrlGen = (url: string, id: number) => `${url}${id}/`
export const getCategoryPost = ({ data }) => parseCategory(data)
export const getCategoryAPI = (id: number): APIConfig<Category> => [
  'GET',
  categoryUrl,
  getCategoryUrlGen,
  basicParamGen,
  getCategoryPost,
  id
]

export const getCategoriesPost = ({ data }) =>
  (data as any[]).map(parseCategory)
export const getCategoriesAPI = (): APIConfig<Category[]> => [
  'GET',
  categoryUrl,
  basicUrlGen,
  basicParamGen,
  getCategoriesPost
]
