// This is a Modified Version of Type definitions for use-global-hook
// Project: https://github.com/andregardi/use-global-hook#readme
// Definitions by: James Hong <https://github.com/ojameso>
// Modified by: HareInWeed <https://github.com/HareInWeed>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 3.6

// Limitation:
// 1. calling other actions from a action without explicit
// type declaration is not type safe currently.

declare module 'use-global-hook' {
  export interface ReactInterface {
    useEffect: (...args: any[]) => any
    useState: (...args: any[]) => any
    useMemo: (...args: any[]) => any
  }

  export interface Store<S, A extends Actions> {
    state: S
    actions: A
    setState(state: Partial<S>, afterUpdateCallback?: () => void): void
  }

  export interface RawAction<S, A extends RawActions<S> = AnyActions> {
    (store: Store<S, A>, ...args: any[]): void
  }
  export interface RawActions<S> {
    [action: string]: RawAction<S> | RawActions<S>
  }

  export interface Action {
    (...args: any[]): void
  }
  export interface Actions {
    [action: string]: Action | Actions
  }

  export interface AnyActions {
    [action: string]: Action & AnyActions
  }

  export type BindAction<A> = A extends RawAction<infer S>
    ? A extends (store: Store<S, infer A>, ...args: infer P) => void
      ? (...args: P) => void
      : never
    : never

  export type BindActions<AS> = AS extends RawActions<infer S>
    ? {
        [P in keyof AS]: AS[P] extends RawActions<infer InnerS>
          ? BindActions<AS[P]>
          : BindAction<AS[P]>
      }
    : never

  export type FreeAction<S, A extends Action, AS extends Actions> = A extends (
    ...args: infer P
  ) => void
    ? (store: Store<S, AS>, ...args: P) => void
    : never

  export type FreeActions<S, AS extends Actions> = {
    [P in keyof AS]: AS[P] extends Actions
      ? FreeActions<S, AS[P]>
      : AS[P] extends Action
      ? FreeAction<S, AS[P], AS>
      : never
  }

  export type InitializerFunction<S, A extends Actions> = (
    store: Store<S, A>
  ) => void

  export type UseGlobal<S, A extends Actions> = (<NS, NA>(
    stateMap: (states: S) => NS,
    actionsMap: (actions: A) => NA
  ) => [NS, NA]) &
    (<NS>(stateMap: (states: S) => NS) => [NS, A]) &
    (<NA>(stateMap: undefined, actionsMap: (actions: A) => NA) => [S, NA]) &
    (() => [S, A])

  export type StoreOfHook<T> = T extends UseGlobal<infer S, infer A>
    ? Store<S, A>
    : never

  function useStore<S, A extends RawActions<S>>(
    React: ReactInterface,
    initialState: S,
    actions: A,
    initializers?: InitializerFunction<S, BindActions<A>>
  ): UseGlobal<S, BindActions<A>>
  function useStore<S, A extends Actions>(
    React: ReactInterface,
    initialState: S,
    actions: FreeActions<S, A>,
    initializers?: InitializerFunction<S, A>
  ): UseGlobal<S, A>
  export default useStore
}
