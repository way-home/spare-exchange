import { FlyResponse } from 'flyio'
import fetch from './fetch'

export type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'MOCK'

export type PostAPI<T, I = any> = (res: FlyResponse<I>) => T | Promise<T>

export type UrlGen = (url: string, ...args: any[]) => string

export const basicUrlGen: UrlGen = (url: string) => url

export type ParamGen = (...args: any[]) => any

export const basicParamGen: ParamGen = () => undefined

export type APIConfig<T, I = any> = [
  Method,
  string, // Url
  UrlGen,
  ParamGen,
  PostAPI<T, I>,
  ...any[] // args
]

export type APIType<API> = API extends APIConfig<infer T>
  ? T
  : API extends (...args: any[]) => APIConfig<infer T>
  ? T
  : never

export const fetcher = <T, I>(...config: APIConfig<T, I>) => {
  const [method, url, urlGen, paramGen, post, ...args] = config
  const fullUrl = urlGen(url, ...args)
  const params = paramGen(...args)
  console.log([method, fullUrl, params])
  return (method === 'MOCK'
    ? Promise.resolve(post(params))
    : fetch
        .request(fullUrl, params, {
          method
        })
        .then(response => post(response))
  ).catch(err => {
    console.error(err)
    throw err
  })
}
