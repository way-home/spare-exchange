export const last = <T>(arr: T[]): T => arr[arr.length - 1]

export function safeLast<T>(arr: T[] | null): T | null
export function safeLast<T>(arr: T[] | null, alter: T): T
export function safeLast<T>(arr: T[] | null, alter: any = null): T | null {
  if (arr == null) {
    return arr
  } else if (arr.length === 0) {
    return alter
  } else {
    return last(arr)
  }
}

export const repeat = <T>(arr: T[], times: number): T[] => {
  return [...Array(times)].reduce((res: T[]) => [...res, ...arr], [] as T[])
}

export const extend = <T>(arr: T[], len: number): T[] => {
  const arrLen = arr.length
  if (arrLen === 0 || arrLen >= len) return arr
  else {
    const times = Math.floor(len / arrLen)
    const rest = len % arrLen
    return [...repeat(arr, times), ...arr.slice(0, rest)]
  }
}

export const scale = <T>(arr: T[], len: number): T[] => {
  const arrLen = arr.length
  if (arrLen === 0) return arr
  else if (arrLen >= len) return arr.slice(0, len)
  else return extend(arr, len)
}

export const pick = <T, K extends keyof T>(
  obj: T,
  ...keys: K[]
): Pick<T, K> => {
  const ret: any = {}
  keys.forEach(key => {
    ret[key] = obj[key]
  })
  return ret
}

export const picker = <K extends keyof any>(...keys: K[]) => <
  T extends Record<K, any>
>(
  obj: T
): Pick<T, K> => pick(obj, ...keys)

export const exactlyPick = <T>(obj: T, ...keys: (keyof any)[]): Partial<T> => {
  const ret = {} as {
    [K in keyof T]: typeof obj[K]
  }
  let key: string
  for (key of Object.keys(obj)) {
    if (keys.includes(key)) {
      ret[key] = obj[key]
    }
  }
  return ret
}

export const exactPicker = (...keys: (keyof any)[]) => <
  T extends Record<keyof any, any>
>(
  obj: T
): Partial<T> => exactlyPick(obj, ...keys)

interface OmitF {
  <T, K extends [...(keyof T)[]]>(obj: T, ...keys: K): {
    [K2 in Exclude<keyof T, K[number]>]: T[K2]
  }
}

export const omit: OmitF = (obj, ...keys) => {
  let ret = {} as {
    [K in keyof typeof obj]: typeof obj[K]
  }
  let key: keyof typeof obj
  for (key in obj) {
    if (!keys.includes(key)) {
      ret[key] = obj[key]
    }
  }
  return ret
}

export const omitter = <K extends keyof any>(...keys: K[]) => <
  T extends Record<K, any>
>(
  obj: T
): Omit<T, K> => omit(obj, ...keys)

export const deduplicate = <T, I>(arr: T[], getKey: (obj: T) => I): T[] => {
  const seen = new Set<I>()
  return arr.filter(el => {
    const duplicate = seen.has(getKey(el))
    seen.add(getKey(el))
    return !duplicate
  })
}

export const filterProps = <T>(
  obj: T,
  filter: (key: keyof T, prop: T[keyof T]) => boolean
) =>
  fromEntries(
    Object.entries(obj).filter(([k, v]) => filter(k as keyof T, v))
  ) as Partial<T>

export const fromEntries = <K extends keyof any, T>(
  entries: [K, T][]
): Record<K, T> => {
  let res: Partial<Record<K, T>> = {}
  for (const [k, v] of entries) {
    res[k] = v
  }
  return res as Record<K, T>
}

type DiffResult<T, N> = {
  [P in keyof T]?: T[P] | N
}
type TestNull<T> = (v: T[keyof T]) => boolean
type TestEqual<T> = (va: T[keyof T], vb: T[keyof T]) => boolean

interface DiffObjectFn {
  <T>(a: T, b: T): DiffResult<T, undefined>
  <T, N>(a: T, b: T, nullVal: N): DiffResult<T, N>
  <T, N>(a: T, b: T, nullVal: N, isNull: TestNull<T>): DiffResult<T, N>
  <T, N>(
    a: T,
    b: T,
    nullVal: N,
    isNull: TestNull<T>,
    isEqual: TestEqual<T>
  ): DiffResult<T, N>
}

export const diffObject: DiffObjectFn = <T, N>(
  a: T,
  b: T,
  nullVal?: N,
  isNull?: (v: T[keyof T]) => boolean,
  isEqual?: (va: T[keyof T], vb: T[keyof T]) => boolean
) => {
  const isNull_: TestNull<T> = isNull != null ? isNull : k => k == null
  const isEqual_: TestEqual<T> = isEqual != null ? isEqual : (a, b) => a === b
  const allKeys = [
    ...new Set([...Object.keys(a), ...Object.keys(b)])
  ] as (keyof T)[]
  const diff: DiffResult<T, N> = {}
  for (const k of allKeys) {
    const va = a[k]
    const vb = b[k]
    if (!isNull_(va)) {
      if (!isNull_(vb)) {
        if (!isEqual_(va, vb)) diff[k] = vb
      } else {
        diff[k] = nullVal
      }
    } else {
      if (!isNull_(vb)) {
        diff[k] = vb
      }
    }
  }
  return diff
}

export const zip = <T>(rows: T[][]) =>
  rows[0].map((_, c) => rows.map(row => row[c]))
