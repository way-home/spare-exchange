import {
  object as yupObject,
  string as yupString,
  number as yupNumber
} from 'yup'

export const fileInfo = yupObject({
  url: yupString(),
  file: yupObject({
    path: yupString(),
    size: yupNumber()
  }).notRequired()
})
