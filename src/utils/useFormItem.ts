import {
  // types
  Dispatch,
  SetStateAction,
  // hooks
  useState
} from '@tarojs/taro'

interface FormItemState<S, F> {
  value: S
  err: boolean
  errMsg: string
  touched: F
}

type Dispatchers<A> = {
  [P in keyof A]: Dispatch<SetStateAction<A[P]>>
}

type FormItemActions<S, F> = Dispatchers<{
  setValue: S
  setErr: boolean
  setErrMsg: string
  setTouched: F
}> & {
  reset: () => void
}

export function useFormItem<S = undefined, F = boolean>(): [
  FormItemState<S, F>,
  FormItemActions<S, F>
]
export function useFormItem<S = undefined, F = boolean>(
  init: undefined,
  flag: F
): [FormItemState<S, F>, FormItemActions<S, F>]
export function useFormItem<S, F = boolean>(
  init: S | (() => S),
  initFlag?: F
): [FormItemState<S, F>, FormItemActions<S, F>]
export function useFormItem<S, F>(
  init?: S,
  initFlag: F = (false as unknown) as F
) {
  const [value, setValue] = useState(init)
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [touched, setTouched] = useState<F>(initFlag)
  const reset = () => {
    setValue(init)
  }
  return [
    {
      value,
      err,
      errMsg,
      touched
    },
    {
      setValue,
      setErr,
      setErrMsg,
      setTouched,
      reset
    }
  ]
}
