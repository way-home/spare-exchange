import { TabItem } from 'taro-ui/@types/tab-bar'

export interface RouteItem {
  pagePath: string[]
  text: string
  icon?: string
}

export type RouteConfig = RouteItem[]

export interface RouteIndex {
  [key: string]: number
}

export const routeConfig2Index = (routeConfig: RouteConfig): RouteIndex => {
  let routeIndex: RouteIndex = {}
  for (let [k, routes] of routeConfig.entries()) {
    for (let path of routes.pagePath) {
      routeIndex[path] = k
    }
  }
  return routeIndex
}

export const routeConfig2TabList = (routeConfig: RouteConfig): TabItem[] => {
  let tabList: TabItem[] = []
  for (let route of routeConfig) {
    tabList.push({
      title: route.text,
      iconType: route.icon
    })
  }
  return tabList
}
