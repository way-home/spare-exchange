export type EchoConfig<T> = {
  pre?: (obj: T) => any
  tag?: string
}

export const echo = <T>(obj: T, { pre, tag }: EchoConfig<T> = {}): T => {
  console.log(`${tag != null ? `${tag}: ` : ''}`, pre != null ? pre(obj) : obj)
  return obj
}

export const echoer = <T>({ pre, tag }: EchoConfig<T> = {}): (<U extends T>(
  obj: U
) => U) => {
  return <U extends T>(obj: U): U => {
    console.log(
      `${tag != null ? `${tag}: ` : ''}`,
      pre != null ? pre(obj) : obj
    )
    return obj
  }
}
