export interface LazyWrapper<T> {
  (): T
}

export type MaybeLazy<T> = T | LazyWrapper<T>

export const isLazyWrapper = <T>(obj: MaybeLazy<T>): obj is LazyWrapper<T> =>
  typeof obj === 'function'

export const resolveLazy = <T>(lazy: MaybeLazy<T>): T =>
  isLazyWrapper(lazy) ? lazy() : lazy

export function resolveLazyOr<T>(lazy: MaybeLazy<T>, alter: T): T
export function resolveLazyOr<T>(
  lazy: MaybeLazy<T>,
  alter?: undefined
): T | null
export function resolveLazyOr(lazy: any, alter: any) {
  try {
    return resolveLazy(lazy)
  } catch (err) {
    return alter || null
  }
}
