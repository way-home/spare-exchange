import Taro, { useRef, useCallback, useEffect } from '@tarojs/taro'

interface UseStableConfigs {
  immediate?: boolean
  unmount?: boolean
  debounce?: boolean
}

export const useStabilizer = (
  time: number,
  { immediate = true, unmount = false, debounce = true }: UseStableConfigs = {}
) => {
  const unmountRef = useRef(unmount)
  const locked = useRef<boolean>(false)
  const timer = useRef<NodeJS.Timeout | null>(null)

  useEffect(() => {
    return () => {
      if (timer.current != null && unmountRef.current)
        clearTimeout(timer.current)
    }
  }, [])

  return useCallback(
    <A extends [...any[]] | any[], R extends any>(
      func: (...args: A) => R
    ): ((...args: A) => R | void) => (...args: A) => {
      const createTimer = () => {
        return setTimeout(() => {
          locked.current = false
          timer.current = null
          if (!immediate) func(...args)
        }, time)
      }

      // set timer
      if (debounce) {
        /* debounce mode */

        // reset timer
        if (timer.current != null) clearTimeout(timer.current)
        timer.current = createTimer()
      } else {
        /* throttle mode */

        if (!locked.current) timer.current = createTimer()
      }

      // invoke function
      if (!locked.current) {
        locked.current = true
        if (immediate) return func(...args)
      }
    },
    [time, immediate, debounce]
  )
}

export const useStable = <A extends [...any[]] | any[], R extends any>(
  func: (...args: A) => R,
  time: number,
  configs: UseStableConfigs = {}
): ((...args: A) => R | void) => {
  return useStabilizer(time, configs)(func)
}
