import useStore, {
  ReactInterface,
  Store,
  Actions,
  RawActions,
  InitializerFunction,
  BindActions,
  FreeActions,
  UseGlobal
} from 'use-global-hook'

export function useGlobalStore<S, A extends RawActions<S>>(
  React: ReactInterface,
  initialState: S,
  actions: A,
  initializers?: InitializerFunction<S, BindActions<A>>
): [UseGlobal<S, BindActions<A>>, Store<S, A>]
export function useGlobalStore<S, A extends Actions>(
  React: ReactInterface,
  initialState: S,
  actions: FreeActions<S, A>,
  initializers?: InitializerFunction<S, A>
): [UseGlobal<S, A>, Store<S, A>] {
  let store: Store<S, A>
  const useHook = useStore(React, initialState, actions, s => {
    store = s
    initializers && initializers(s)
  })
  return [useHook, store!]
}
