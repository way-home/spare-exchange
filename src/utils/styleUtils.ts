export const px = process.env.TARO_ENV === 'weapp' ? 'rpx' : 'px'

export const customAvatarSize = (size: number) =>
  `height:${size}${px}; width:${size}${px}; font-size:${size /
    2.5}${px}; line-height:${size}${px};`
