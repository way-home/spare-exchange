import Taro from '@tarojs/taro'
import configs from '@/configs'

export const numEnumKeys = (enum_: object): number[] => {
  return Object.values(enum_).filter(e => typeof e === 'number')
}

export const numEnumValues = (enum_: object): string[] => {
  return Object.values(enum_).filter(e => typeof e !== 'number')
}

export const sleep = (ms: number): Promise<void> => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export const priceRegex = /^\d*(\.\d{1,2})?$/

export const telRegex = /^(((\+?\d{1,4}-)?(\d{11}|\d{7,9}|\d{1,4}-\d{7,9}))|\(\d{1,4}\)\d{7,9})$/

// the code below doesn't work under Wechat on Android
// export const zhCompare = new Intl.Collator('zh-Hans-CN').compare

// although the code below doesn't behave exactly the same in every platform,
// it's the best implement without farther workaround for now
export const zhCompare = (a: string, b: string): number =>
  a.localeCompare(b, 'zh-Hans-CN')

export const setAdd = <T>(s: Set<T>, e: T): Set<T> => {
  s.add(e)
  return new Set<T>(s)
}

export const setDelete = <T>(s: Set<T>, e: T): Set<T> => {
  s.delete(e)
  return new Set<T>(s)
}

type ParamsType = Record<
  string,
  Parameters<typeof encodeURIComponent>[0] | undefined | null
>

export const encodeParams = (params: ParamsType): string => {
  const queries = Object.entries(params)
    .filter(([_, value]) => value !== undefined)
    .map(pair => pair.map(encodeURIComponent))
    .map(([name, value]) => `${name}=${value}`)
    .join('&')
  return queries !== '' ? `?${queries}` : ''
}

type WebPageParams = {
  title?: string
  url?: string
}

export const gotoWebpage = async (params: WebPageParams) =>
  Taro.navigateTo({
    url: `/pages/webview/index${encodeParams(params)}`
  })

export const num2money = (num: number): string =>
  num.toFixed(2).replace(/\.?0+$/, '')

export const string2currency = (num: string, currency: string = '￥'): string =>
  `${currency}${num}`

export const num2discount = (num: number): string =>
  `${(10 * num).toFixed(1).replace(/\.?0+$/, '')}折`

export const parseUrl = (url: string): string => {
  const url_ = url.replace(/^\s+/, '')
  if (url_.match(/^\w+:\/\//)) {
    return url_
  } else {
    return `${configs.addr()}${url_.match(/^\//) ? '' : '/'}${url_}`
  }
}
