import {
  useState,
  useDidShow,
  EffectCallback,
  DependencyList
} from '@tarojs/taro'

export enum LoadStatus {
  notLoaded,
  loading,
  loaded,
  error
}

export const useLoadGuard = (
  useHook: (effect: EffectCallback, deps?: DependencyList) => void = useDidShow,
  retryTimes: number = 0
) => {
  const [status, setStatus] = useState(LoadStatus.notLoaded)
  const [error, setError] = useState<any>(null)
  const [times, setTimes] = useState(0)

  const clearStatus = () => {
    setStatus(LoadStatus.notLoaded)
    setTimes(0)
    setError(null)
  }

  const useGuard = (func: () => Promise<void>, deps: DependencyList = []) => {
    useHook(() => {
      if (status !== LoadStatus.loading) {
        setStatus(LoadStatus.loading)
        func()
          .then(() => setStatus(LoadStatus.loaded))
          .catch(err => {
            if (retryTimes < 0 || times < retryTimes) {
              setTimes(t => t + 1)
              setStatus(LoadStatus.notLoaded)
            } else {
              setError(err)
              setStatus(LoadStatus.error)
            }
          })
      }
    }, deps)
  }

  return {
    error,
    status,
    clearStatus,
    guard: useGuard,
    useGuard,
    is: {
      notLoaded: () => status === LoadStatus.notLoaded,
      loading: () => status === LoadStatus.loading,
      loaded: () => status === LoadStatus.loaded,
      error: () => status === LoadStatus.error
    }
  }
}
