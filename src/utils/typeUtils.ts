export type Nullable<T> = {
  [K in keyof T]: T[K] | null
}

export type PartialNullable<T> = {
  [K in keyof T]?: T[K] | null
}

export type AlterType<T, S, D> = S extends T ? Exclude<T, S> | D : T

export type AlterPropsType<T, S, D> = {
  [K in keyof T]: AlterType<T[K], S, D>
}

export type Partial2Nullable<T> = AlterPropsType<T, undefined, null>

export type ResolvePromise<T> = T extends Promise<infer R> ? R : never

export type UnionToIntersection<U> = (U extends any
? (k: U) => void
: never) extends (k: infer I) => void
  ? I
  : never

export type PartialExclude<T, K extends keyof T> = Partial<Omit<T, K>> &
  { [P in K]: T[P] }

export type PartialNullableExclude<T, K extends keyof T> = PartialNullable<
  Omit<T, K>
> &
  { [P in K]: T[P] }
