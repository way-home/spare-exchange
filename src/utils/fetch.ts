import Taro from '@tarojs/taro'
import configs from '@/configs'
import { Fly as FlyT } from 'flyio'
import Fly from 'flyio/dist/npm/wx'

let fly: FlyT = new Fly()

fly.config = {
  ...fly.config,
  baseURL: configs.addr(),
  withCredentials: true
}

const extractCookie = (cookieSpec: string): string => {
  return cookieSpec.split(';')[0]
}

const setCookie = (cookieSpec: string): void => {
  const [name, value] = extractCookie(cookieSpec).split('=')
  if (value === '""') Taro.removeStorageSync(name)
  else Taro.setStorageSync(name, value)
}

const makeCookies = (names: string[]): string => {
  let cookies: string[] = []
  for (const name of names) {
    const value = Taro.getStorageSync(name)
    if (value) {
      cookies.push(`${name}=${value}`)
    }
  }
  return cookies.join('; ')
}

export const defaultCookieGen = () => makeCookies(['csrftoken', 'session_id'])

fly.interceptors.request.use(req => {
  try {
    if (req.method !== 'GET') {
      const csrftoken = Taro.getStorageSync('csrftoken')

      if (csrftoken === '')
        console.warn(`a ${req.method} request without csrftoken`)
      else req.headers['X-CSRFToken'] = csrftoken
    }
    if (req.method === 'PATCH') {
      req.method = 'POST'
      req.url = '/patch-proxy' + req.url
    }
    req.headers.cookie = defaultCookieGen()
  } catch (err) {
    console.log(err)
  }
  return req
})

fly.interceptors.response.use(
  res => {
    // console.log(res)
    const cookies = res.headers['set-cookie']
    // console.log(cookies)
    if (cookies) {
      // lock fly if necessary
      fly.interceptors.response.lock()
      // console.log('cookie: ', cookies)
      cookies.map(setCookie)
      fly.interceptors.response.unlock()
    } else {
      // console.log('no cookie')
    }
    return res
  },
  err => {
    console.error(err)
  }
)

const fetch = fly

export default fetch
