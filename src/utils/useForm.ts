import Taro, {
  useState,
  useCallback,
  useMemo,
  Dispatch,
  SetStateAction
} from '@tarojs/taro'
import { Schema as YupSchema, ValidateOptions, ValidationError } from 'yup'

export type KeyType = string | number | symbol

// export type BasicForm = Record<string, any>

// due to typescript limitations, it is impossible to get the
// corresponding value of a field with nested path (or so called name)
export type GetValue<Name, Values> = Name extends keyof Values
  ? Values[Name]
  : unknown

export type FormErrors<Names extends KeyType, Error = string> = {
  [K in Names]: Error | null
}

export type PartialErrors<Names extends KeyType, Error = string> = Partial<
  {
    [K in Names]: Error
  }
>

export type ErrorInfo<Names extends KeyType, Error = string> = {
  [K in Names]: Error
}

// TODO: improve `ValidateSchema` by type checking `name` as `Names` in `useForm`
export type ValidateSchema<Values, Error = string> = {
  // the error object that `validate` throws must meet `Partial<ErrorInfo>`
  validate: (values: Values) => Promise<any>
  validateAt: (name: string, values: Values) => Promise<any>
}

export type FieldInfo<
  Name extends Names,
  Names extends KeyType,
  Value,
  Values,
  Error = string
> = {
  name: Name
  value: Value
  error: Error | null
  formDisabled: boolean
  setValue: Dispatch<Value>
  setValues: Dispatch<Partial<Values>>
  setValueAndValidate: Dispatch<Partial<Values>>
  validate: () => Promise<Error | null>
  validateGroup: (names: Names[]) => Promise<Partial<ErrorInfo<Names, Error>>>
  validateAll: (names: Names[]) => Promise<Partial<ErrorInfo<Names, Error>>>
  setError: Dispatch<Error | null>
  setErrors: Dispatch<Partial<FormErrors<Names, Error>>>
}

export type ComponentAdapter<
  Name extends Names,
  Names extends KeyType,
  Value,
  Values,
  Props,
  Error = string
> = (config: FieldInfo<Name, Names, Value, Values, Error>) => Props

export type GenericComponentAdapter<
  Names extends KeyType,
  Values,
  Error = string
> = <Name extends Names>(
  config: FieldInfo<Name, Names, GetValue<Name, Values>, Values, Error>
) => any

export type FallbackProps<Name, Value, Error = string> = {
  name: Name
  onBlur: (...args: any) => void
  onChange: (v: Value) => void
  disabled: boolean
  value: Value
}

export const fallbackAdapter = <
  Name extends Names,
  Names extends KeyType,
  Value,
  Values,
  Error = string
>({
  name,
  value,
  formDisabled,
  setValue,
  validate
}: FieldInfo<Name, Names, Value, Values, Error>): FallbackProps<
  Name,
  Value,
  Error
> => ({
  name: name,
  value: value,
  disabled: formDisabled,
  onBlur: () => validate().catch(() => {}),
  onChange: value => setValue(value)
})

export type useFormConfig<
  Values,
  Names extends KeyType,
  Error = string,
  DefaultProps extends object = FallbackProps<
    Names,
    GetValue<Names, Values>,
    Error
  >
> = {
  initialData: Values
  onSubmit: (onSubmitArgs: {
    values: Values
    errors: FormErrors<Names, Error>
    validate: (name?: Names) => Promise<Values>
    setSubmitting: Dispatch<SetStateAction<boolean>>
    setValues: Dispatch<Partial<Values>>
    setErrors: Dispatch<Partial<FormErrors<Names, Error>>>
    resetValues: () => void
    resetErrors: () => void
    resetForm: () => void
    onError: (errors: Partial<PartialErrors<Names, Error>>) => void
  }) => void | Promise<void>
  names?: Names[]
  validateBeforeSubmit?: boolean
  disabledDuringSubmitting?: boolean
  validateSchema?: ValidateSchema<Values, Error>
  onError?: (errors: Partial<PartialErrors<Names, Error>>) => void
  defaultAdapter?: ComponentAdapter<
    Names,
    Names,
    GetValue<Names, Values>,
    Values,
    DefaultProps,
    Error
  >
}

export const useForm = <
  Values,
  Names extends KeyType = keyof Values,
  Error = string,
  DefaultProps extends object = {}
>(
  configs: useFormConfig<Values, Names, Error, DefaultProps>
) => {
  // get parameters
  const {
    initialData,
    onSubmit,
    names: _names,
    validateBeforeSubmit = true,
    disabledDuringSubmitting = true,
    validateSchema,
    onError,
    defaultAdapter = fallbackAdapter
  } = configs
  const names: Names[] =
    _names != null ? _names : (Object.keys(initialData) as Names[])
  // use state
  const [formDisabled, setFormDisabled] = useState(false)
  const initialErrors = useMemo(() => {
    let initialError: FormErrors<Names, Error> = {} as FormErrors<Names, Error>
    for (const key of names) {
      initialError[key] = null
    }
    return initialError
  }, names)
  const [values, setValues] = useState(initialData)
  const [errors, setErrors] = useState(initialErrors)
  // callbacks
  const resetValues = useCallback(() => {
    setValues(initialData)
  }, [initialData])
  const resetErrors = useCallback(() => {
    setErrors(initialErrors)
  }, [initialErrors])
  const resetForm = useCallback(() => {
    setValues(initialData)
    setErrors(initialErrors)
  }, [initialData, initialErrors])
  const setValue = useCallback(
    (name: Names, value: GetValue<typeof name, Values>) => {
      setValues(values => ({
        ...values,
        [name]: value
      }))
    },
    [setValues]
  )
  const setError = useCallback(
    (name: Names, error: Error | null) => {
      setErrors(values => ({
        ...values,
        [name]: error
      }))
    },
    [setErrors]
  )
  const validateValuesAt = useCallback(
    (name: Names, values: Values) =>
      validateSchema
        ? validateSchema
            .validateAt(name.toString(), values)
            .then(values => {
              setError(name, null)
              return values
            })
            .catch(err => {
              setError(name, err)
              const error: Partial<PartialErrors<Names, Error>> = {}
              error[name] = err
              onError && onError(error)
              throw err
            })
        : Promise.resolve(values),
    [setError, validateSchema, onError]
  )
  const validateAt = useCallback(
    (name: Names) => validateValuesAt(name, values),
    [values, validateValuesAt]
  )
  const setValueAndValidate = useCallback(
    (name: Names, value: GetValue<typeof name, Values>) => {
      setValues(values => {
        const newValues = {
          ...values,
          [name]: value
        }
        validateValuesAt(name, newValues)
        return newValues
      })
    },
    [setValues, validateValuesAt]
  )

  const validateGroup = useCallback(
    (names: Names[]) =>
      validateSchema
        ? Promise.all(
            names.map(name =>
              validateSchema
                .validateAt(name.toString(), values)
                .then(() => null)
                .catch(err => [name, err])
            )
          )
            .then(errors => errors.filter(err => err != null))
            .then(errors => {
              if (errors.length === 0) return values
              const errObj: Partial<PartialErrors<Names, Error>> = (errors as [
                keyof Values,
                Error
              ][]).reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {})
              onError && onError(errObj)
              throw errObj
            })
        : Promise.resolve(values),
    [values, setError, validateSchema, onError]
  )
  const validateAll = useCallback(
    () =>
      validateSchema
        ? validateSchema
            .validate(values)
            .then(values => {
              resetErrors()
              return values
            })
            .catch(errs => {
              setErrors({ ...initialErrors, ...errs })
              onError && onError(errs)
              throw errs
            })
        : Promise.resolve(values),
    [values, setError, validateSchema, onError]
  )
  const validate = useCallback(
    (name?: Names) => {
      if (name != null) {
        return validateAt(name)
      } else {
        return validateAll()
      }
    },
    [values, validateAt, validateAll]
  )

  interface Register {
    // due to typescript limitation, we can not get the return type
    // from a generic function, thus the return value is not type
    // safe here
    <Name extends Names>(name: Name): {} extends DefaultProps
      ? FallbackProps<Name, GetValue<Name, Values>, Error>
      : any
    <Name extends Names, Props extends object>(
      name: Name,
      adapter: ComponentAdapter<
        Name,
        Names,
        GetValue<Name, Values>,
        Values,
        Props,
        Error
      >
    ): Props
  }

  const register: Register = <Name extends Names, Props extends object>(
    name: Name,
    adapter?: ComponentAdapter<
      Name,
      Names,
      GetValue<Name, Values>,
      Values,
      Props,
      Error
    >
  ) => {
    const config: FieldInfo<
      Name,
      Names,
      GetValue<Name, Values>,
      Values,
      Error
    > = {
      name: name,
      // @ts-ignore
      value: values[name], // TODO: get value recursively by name
      error: errors[name],
      formDisabled: formDisabled,
      setValue: setValue.bind(null, name),
      setValues: v => setValues(val => ({ ...val, ...v })),
      setError: setError.bind(null, name),
      setErrors: e => setErrors(err => ({ ...err, ...e })),
      validate: validateAt.bind(null, name),
      setValueAndValidate: setValueAndValidate.bind(null, name),
      validateGroup: validateGroup,
      validateAll: validateAll
    }
    return adapter != null ? adapter(config) : defaultAdapter(config)
  }

  const handleSubmit = () => {
    if (formDisabled) return
    ;(validateBeforeSubmit
      ? validateAll()
          .then(() => true)
          .catch(() => false)
      : Promise.resolve(true)
    )
      .then(noError => {
        if (!noError) return
        if (disabledDuringSubmitting) setFormDisabled(true)
        return onSubmit({
          values: values,
          errors: errors,
          setValues: partialValues =>
            setValues(values => ({ ...values, ...partialValues })),
          setErrors: partialErrors =>
            setErrors(errors => ({ ...errors, ...partialErrors })),
          setSubmitting: setFormDisabled,
          resetValues,
          resetErrors,
          resetForm,
          onError: onError != null ? onError : () => {},
          validate
        })
      })
      .finally(() => {
        if (disabledDuringSubmitting) setFormDisabled(false)
      })
  }
  return {
    values,
    errors,
    register,
    handleSubmit,
    formDisabled,
    setFormDisabled,
    resetForm,
    setValue,
    setValues,
    resetValues,
    setError,
    setErrors,
    resetErrors,
    setValueAndValidate,
    validate,
    validateGroup
  }
}

export const yupSchema = <T>(
  schema: YupSchema<T>,
  optionInSingle?: ValidateOptions,
  optionInAll: ValidateOptions = { abortEarly: false }
): ValidateSchema<T> => {
  return {
    validateAt: (name: string, values: T) =>
      schema
        .validateAt(
          name,
          values,
          optionInSingle != null ? optionInSingle : undefined
        )
        .catch(err => {
          throw err.message
        }),
    validate: (values: T) =>
      schema
        .validate(values, optionInAll != null ? optionInAll : undefined)
        .catch(err => {
          let errors = {}
          ;(err as ValidationError).inner.map(
            error => (errors[error.path] = error.message)
          )
          throw errors
        })
  }
}
