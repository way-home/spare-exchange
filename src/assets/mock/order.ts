import { Order, getOrderUrl, OrderStatus, defaultOrder } from '@/models/order'
import { APIConfig, basicUrlGen, basicParamGen } from '@/utils/APIFetcher'
import { sleep } from '@/utils'
import { mockGoods } from './good'

const partialMockOrders: Partial<Order>[] = [
  { status: OrderStatus.Success, good: mockGoods[0].id },
  { status: OrderStatus.OnGoing, good: mockGoods[1].id },
  { status: OrderStatus.CancelledBySeller, good: mockGoods[2].id },
  { status: OrderStatus.CancelledByBuyer, good: mockGoods[3].id }
]

export const mockOrders: Order[] = partialMockOrders.map((po, id) => ({
  ...defaultOrder,
  ...po,
  id
}))

export const mockOrder: Order = mockOrders[0]

export const getOrderUrlGen = (url, id: Order['id']) => `${url}/${id}`
export const getOrderParamsGen = (id: Order['id']) => ({ id })
export const getOrderPost = async params => {
  await sleep(300)
  return mockOrders.find(o => o.id == params.id) as Order
}
export const getOrderAPI = (id: Order['id']): APIConfig<Order> => [
  'MOCK',
  getOrderUrl,
  getOrderUrlGen,
  basicParamGen,
  getOrderPost,
  id
]

export const getOrdersPost = async () => {
  await sleep(300)
  return mockOrders
}
export const getOrdersAPI = (): APIConfig<Order[]> => [
  'MOCK',
  getOrderUrl,
  basicUrlGen,
  basicParamGen,
  getOrdersPost
]
