import { basicUrlGen, basicParamGen, APIConfig } from '@/utils/APIFetcher'
import { Category, categoryUrl } from '@/models/category'
import { sleep } from '@/utils'

export const mockCategories: Category[] = [
  '图书',
  '个护美装',
  '券/票/会员/服务卡',
  '水果零食',
  '服装',
  '3C数码',
  '小家电',
  '其他闲置'
].map((description, id) => ({
  id,
  parent: null,
  description
}))

export const getCategoriesPost = async () => {
  await sleep(500)
  return mockCategories
}
export const getCategoriesAPI = (): APIConfig<Category[]> => [
  'MOCK',
  categoryUrl,
  basicUrlGen,
  basicParamGen,
  getCategoriesPost
]

export const getCategoryParamsGen = (id: number) => ({ id })
export const getCategoryPost = param =>
  mockCategories.find(c => c.id === param.id) as Category
export const getCategoryAPI = (id: number): APIConfig<Category> => [
  'MOCK',
  categoryUrl,
  basicUrlGen,
  getCategoryParamsGen,
  getCategoryPost,
  id
]
