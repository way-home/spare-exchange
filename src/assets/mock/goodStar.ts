import { GoodStar, defaultGoodStar } from '@/models/goodStar'
import { mockGoods } from './good'

export const mockGoodStars: GoodStar[] = mockGoods.map(good => ({
  ...defaultGoodStar,
  good
}))

export const mockGoodStar: GoodStar = mockGoodStars[0]
