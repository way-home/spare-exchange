import { Good, goodUrl, defaultGood, defaultUser } from '@/models'
import { basicUrlGen, basicParamGen, APIConfig } from '@/utils/APIFetcher'
import { mockCategories } from './category'
import { mockUser } from './user'
import { book, book2, headphone, coupon } from './img'
import { extend, sleep } from '@/utils'

const partialMockGoods: Partial<Good>[] = [
  {
    name: '奔驰人生 唐家红星 1.29 三点场影票',
    category: mockCategories.find(c => c.description.match(/图书/)),
    description: '有事去不了了，临时出电影票，有意者来',
    images: [coupon],
    current_price: '20'
    // original_price: 33,
    // discount: 3.0
  },
  {
    name: '森海塞尔 MOMENTUM 蓝牙无线包耳式耳机',
    category: mockCategories.find(c => c.description.match(/数码/)),
    description: '九成新，基本没怎么用，音质还可以，我戴起来不舒服所以出了',
    images: [headphone],
    original_price: '2999',
    current_price: '2500',
    owner: mockUser.id
  },
  {
    name: '数学分析简明教程（上册）',
    category: mockCategories.find(c => c.description.match(/书/)),
    description: '毕业了出课本，带笔记，七成新',
    images: [book, book2],
    original_price: '30',
    current_price: '15',
    press: '高等教育出版社',
    author: '邓东皋',
    isbn: '9787562900917'
  },
  {
    name: '数学分析简明教程（下册）',
    category: mockCategories.find(c => c.description.match(/书/)),
    description: '毕业了出课本，带笔记，七成新',
    original_price: '30',
    current_price: '9',
    press: '高等教育出版社',
    author: '邓东皋',
    isbn: '9787040199543'
  }
]

export const mockGoods: Good[] = extend(partialMockGoods, 100).map(
  (d, idx) => ({
    ...defaultGood,
    id: idx,
    ...d
  })
)

export const mockGood: Good = mockGoods[0]

export const getGoodsPost = async () => {
  await sleep(500)
  return mockGoods
}
export const getGoodsAPI = (): APIConfig<Good[]> => [
  'MOCK',
  goodUrl,
  basicUrlGen,
  basicParamGen,
  getGoodsPost
]

export const getUserGoodsPost = async params => {
  await sleep(300)
  const user = params.user
  return mockGoods.filter(g => g.owner === user)
}
export const getUserGoodsParamsGen = (user: number) => ({ user })
export const getUserGoodsAPI = (user: number): APIConfig<Good[]> => [
  'MOCK',
  goodUrl,
  basicUrlGen,
  getUserGoodsParamsGen,
  getUserGoodsPost,
  user
]

export const getGoodParamGen = id => ({ id })
export const getGoodPost = async params => {
  await sleep(500)
  return mockGoods.find(g => g.id === params.id)
}
export const getGoodAPI = (id: number): APIConfig<Good | undefined> => [
  'MOCK',
  goodUrl,
  basicUrlGen,
  getGoodParamGen,
  getGoodPost,
  id
]
