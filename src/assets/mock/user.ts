import {
  User,
  BasicUser,
  defaultUser,
  defaultBasicUser,
  personalDataUrl,
  userDataUrl
} from '@/models/user'
import { APIConfig, basicUrlGen, basicParamGen } from '@/utils/APIFetcher'
import { sleep } from '@/utils'

export const mockBasicUser: BasicUser = {
  ...defaultBasicUser,
  wx_info: {
    id: 0,
    nickName: 'nickname'
  },
  user_info: {
    id: 0,
    number: '12341234',
    name: 'name',
    college: 'college'
  }
}

export const mockUser: User = {
  ...defaultUser,
  id: 1,
  user_info: { ...mockBasicUser.user_info, id: 1 }
}

export const getMyInfoPost = async () => {
  await sleep(300)
  return mockUser
}
export const getMyInfoAPI = (): APIConfig<User> => [
  'MOCK',
  personalDataUrl,
  basicUrlGen,
  basicParamGen,
  getMyInfoPost
]

export const getUserInfoParamsGen = (id: number) => ({ id })
export const getUserInfoPost = async (params): Promise<BasicUser> => {
  const id: number = params.id
  await sleep(300)
  return {
    ...mockBasicUser,
    id,
    wx_info: {
      ...mockBasicUser.wx_info,
      id,
      nickName: `${mockBasicUser.wx_info.nickName}_${id}`
    },
    user_info: {
      ...mockBasicUser.user_info,
      id
    }
  }
}
export const getUserInfoAPI = (id: BasicUser['id']): APIConfig<BasicUser> => [
  'MOCK',
  userDataUrl,
  basicUrlGen,
  getUserInfoParamsGen,
  getUserInfoPost,
  id
]
