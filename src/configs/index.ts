import { Configs } from './configs.declares'

import { privateConfigs } from './configs.private'
import { commonConfigs } from './configs.common'

const configs: Configs = {
  ...commonConfigs,
  ...privateConfigs,
  addr: function() {
    return `${this.scheme}://${this.host}${
      this.port != null ? `:${this.port}` : ''
    }`
  }
}

export default configs
