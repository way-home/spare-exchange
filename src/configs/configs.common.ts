import { CommonConfigs } from './configs.declares'

export const commonConfigs: CommonConfigs = {
  scheme: 'http',
  host: 'localhost',
  port: 9000
}
