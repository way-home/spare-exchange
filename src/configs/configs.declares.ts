import { AlterPropsType } from '@/utils'

export interface ServerInfoData {}

export interface CommonConfigs {
  scheme: 'http' | 'https'
  host: string
  port?: number
}

export interface PrivateConfigs
  extends ServerInfoData,
    Partial<AlterPropsType<CommonConfigs, undefined, undefined | null>> {}

export interface Configs
  extends AlterPropsType<CommonConfigs, undefined, undefined | null>,
    Omit<PrivateConfigs, keyof CommonConfigs> {
  addr: () => string
}
