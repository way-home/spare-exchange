import { createContext } from '@tarojs/taro'

import { ConfigInterface } from './types'

const SWRConfigContext = createContext<ConfigInterface>({})
SWRConfigContext.displayName = 'SWRConfigContext'

export default SWRConfigContext
