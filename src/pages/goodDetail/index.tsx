import Taro, {
  FC,
  useRouter,
  useShareAppMessage,
  useEffect,
  useMemo
} from '@tarojs/taro'
import { View, Button, Block } from '@tarojs/components'
import { GoodCard, UserCard, Divider, Modal } from '@/components'
import { useModalState, useShowModal } from '@/components/modal/useModalState'
import ModalContext from './modal'
import SellerFooter from './sellerFooter'
import BuyerFooter from './buyerFooter'

import { useUserStore, LoginStatus } from '@/store'

import {
  Good,
  BasicUser,
  Order,
  getGoodAPI,
  getOrdersAPI,
  getUserInfoAPI,
  GoodStatus,
  confirmOrder,
  cancelOrder,
  OrderStatus
} from '@/models'
import useSWR from 'swr'

import { encodeParams, useStabilizer } from '@/utils'
import format from 'date-fns/format'

import classNames from 'classnames'

import './index.scss'

type ModeType = 'top' | 'orders'

const GoodDetailPage: FC = () => {
  const { params } = useRouter()
  const id: number = parseInt(params.id)
  const mode: ModeType = (params.dist as ModeType) || 'top'

  const [{ loginStatus, id: me }] = useUserStore()
  const { data: good, error: goodErr, revalidate } = useSWR<Good>(
    id != null ? getGoodAPI(id) : null,
    undefined,
    {
      onErrorRetry: (
        error,
        key,
        { errorRetryInterval },
        revalidate,
        { retryCount }
      ) => {
        const count = retryCount || 0
        if (count >= 10) return
        if (error.status === 404) return

        // retry after 5 seconds
        setTimeout(
          () => revalidate({ retryCount: count + 1 }),
          errorRetryInterval
        )
      }
    }
  )

  const authorized = loginStatus === LoginStatus.Authorized
  const logging = loginStatus === LoginStatus.Pending
  const imSeller = authorized && good != null && good.owner === me
  const forSell = good != null && good.status === GoodStatus.ForSell

  // if i'm not seller:
  const { data: seller } = useSWR<BasicUser>(
    good != null && !imSeller ? getUserInfoAPI(good.owner) : null
  )
  // otherwise:
  const { data: allOrders, mutate: refresh } = useSWR<Order[]>(
    good != null && imSeller ? getOrdersAPI({ good: good.id }) : null
  )
  const orders = useMemo(
    () =>
      allOrders == null
        ? allOrders
        : allOrders.filter(({ status }) => status === OrderStatus.OnGoing),
    [allOrders]
  )

  const [modalState, modalActions] = useModalState()
  const showModal = useShowModal(modalActions)

  useEffect(() => {
    if (imSeller && mode === 'orders') {
      Taro.pageScrollTo({ selector: '#buyers' })
    }
  }, [mode, imSeller])

  const status = goodErr && goodErr.response && goodErr.response.status

  useEffect(() => {
    // if good not found, redirect back to home page after 3 second
    if (goodErr != null) {
      console.error(goodErr)
      if (status === 404) {
        setTimeout(() => Taro.switchTab({ url: '/pages/home/index' }), 3000)
      }
    }
  }, [goodErr, status])

  useShareAppMessage(() => {
    if (good) {
      return {
        title: good.name,
        path: `/pages/goodDetail/index?id=${id}`
      }
    }
  })

  const viewUserStb = useStabilizer(300)
  const confirmOrderStb = useStabilizer(300)
  const cancelOrderStb = useStabilizer(300)

  return (
    <View
      className={classNames('good-detail', {
        'good-detail--not-found': status === 404
      })}
    >
      <ModalContext.Provider value={showModal}>
        {good != null && goodErr == null && (
          <Block>
            <View className="good-detail__good">
              <GoodCard
                good={good}
                orientation="vertical"
                contentLevel="detailed"
              />
            </View>

            {authorized && imSeller && forSell && (
              <Block>
                <Divider top bottom />
                <View className="good-detail-orders" id="buyers">
                  <View className="good-detail-orders__title">欲购买用户</View>
                  {/* workaround for `&&` operator not able to use here, no clue for why */}
                  {!!orders &&
                    (orders != null ? orders : []).map(o => (
                      <View key={o.buyer} className="good-detail-order">
                        <UserCard userId={o.buyer} detailLayout="flow">
                          <View className="good-detail-order__time">
                            {format(o.start_time, 'yyyy-MM-dd')}
                          </View>
                          <View className="good-detail-order__actions">
                            <Button
                              className="good-detail-order__action"
                              onClick={viewUserStb(() => {
                                Taro.navigateTo({
                                  url: `/pages/userInfo/index${encodeParams({
                                    mode: 'contact',
                                    id: o.buyer
                                  })}`
                                })
                              })}
                            >
                              查看用户信息
                            </Button>
                            <Button
                              className="good-detail-order__action"
                              onClick={confirmOrderStb(async () => {
                                // TODO: add modal
                                await confirmOrder(o.id)
                                await refresh(
                                  orders.filter(order => order.id !== o.id),
                                  false
                                )
                              })}
                            >
                              确认成交
                            </Button>
                            <Button
                              className="good-detail-order__action"
                              onClick={cancelOrderStb(async () => {
                                // TODO: add modal
                                await cancelOrder(o.id)
                                await refresh(
                                  orders.filter(order => order.id !== o.id),
                                  false
                                )
                              })}
                            >
                              取消订单
                            </Button>
                          </View>
                        </UserCard>
                      </View>
                    ))}
                  <View className="good-detail-orders__bottom">
                    似乎没有更多订单了，
                    <Button
                      className="good-detail-orders__bottom-share"
                      openType="share"
                    >
                      <View className="at-icon at-icon-share"></View> 分享
                    </Button>
                    一下，让更多的人看到您的商品吧！
                  </View>
                </View>
              </Block>
            )}
            {!imSeller && (
              <Block>
                <Divider top bottom />
                <UserCard user={seller} size="medium" contentLevel="detailed">
                  <Button
                    className="user-card-action"
                    onClick={() => {
                      Taro.navigateTo({
                        url: `/pages/userHome/index${encodeParams({
                          id: good ? good.owner : undefined
                        })}`
                      })
                    }}
                  >
                    查看主页
                  </Button>
                </UserCard>
                <Divider top bottom />
              </Block>
            )}
            <View className="good-detail__footer">
              {authorized && good != null && good.owner === me ? (
                <SellerFooter good={good} revalidate={revalidate} />
              ) : (
                <BuyerFooter good={good} revalidate={revalidate} />
              )}
            </View>
          </Block>
        )}
        {status === 404 && (
          <View className="good-not-found">
            <View className="good-not-found__content">
              <View className="at-icon at-icon-alert-circle good-not-found__icon"></View>
              <View className="good-not-found__msg">
                <View className="good-not-found__msg-text">
                  我们没能找到您要访问的物品
                </View>
                <View className="good-not-found__note">
                  3秒后将自动返回主页
                </View>
              </View>
            </View>
          </View>
        )}
      </ModalContext.Provider>
      <Modal state={modalState} />
    </View>
  )
}

GoodDetailPage.config = {
  navigationBarTitleText: '物品详情'
}

export default GoodDetailPage
