import Taro, { FC, useContext } from '@tarojs/taro'
import { View, Button, Block } from '@tarojs/components'

import ModalContext from '../modal'

import {
  // Good
  Good,
  updateGood,
  GoodStatus
} from '@/models'

import { encodeParams, useStabilizer } from '@/utils'

import './index.scss'

interface SellerFooterProps {
  good?: Good
  revalidate: () => Promise<boolean>
}

type StatusButton = 'remove' | 'add' | 'new'

const statusButtonList: Record<GoodStatus, StatusButton> = {
  [GoodStatus.Unknown]: 'remove', // fallback to 'remove'
  [GoodStatus.ForSell]: 'remove',
  [GoodStatus.NotAvailable]: 'add',
  [GoodStatus.Sold]: 'new'
}

const SellerFooter: FC<SellerFooterProps> = ({ good, revalidate }) => {
  // user info
  // const [{ loginStatus, id: me }] = useUserStore()
  // login status are not used in here since we assume using this component requires authorization
  // this is guaranteed by the code in the parent component/page, i.e. , `goodDetail`

  const showModal = useContext(ModalContext)

  const statusButton = (good && statusButtonList[good.status]) || 'remove'

  const editStabilizer = useStabilizer(300)
  const statusStabilizer = useStabilizer(300)

  return (
    <View className="footer">
      {good && (
        <Block>
          <Button className="footer__share" openType="share">
            <View className="at-icon at-icon-share footer__icon" />
            分享
          </Button>
          {good.status === GoodStatus.ForSell && (
            <View
              className="footer__edit"
              onClick={editStabilizer(() => {
                Taro.navigateTo({
                  url: `/pages/goodForm/index${encodeParams({
                    mode: 'edit',
                    id: good.id
                  })}`
                })
              })}
            >
              <View className="at-icon at-icon-edit footer__icon" />
              编辑
            </View>
          )}
          {good.status === GoodStatus.NotAvailable && (
            <View
              className="footer__edit"
              onClick={editStabilizer(() => {
                Taro.navigateTo({
                  url: `/pages/goodForm/index${encodeParams({
                    mode: 'edit-available',
                    id: good.id
                  })}`
                })
              })}
            >
              <View className="at-icon at-icon-edit footer__icon" />
              编辑并上架
            </View>
          )}
          {/* status button: */}
          {statusButton === 'add' && (
            <View
              className="footer__status"
              onClick={statusStabilizer(async () => {
                await updateGood({
                  id: good.id,
                  status: GoodStatus.ForSell
                })
                await revalidate()
              })}
            >
              <View className="at-icon at-icon-share-2 footer__icon" />
              上架
            </View>
          )}
          {statusButton === 'new' && (
            <View
              className="footer__status footer__status--new"
              onClick={statusStabilizer(() => {
                Taro.navigateTo({
                  url: `/pages/goodForm/index${encodeParams({
                    mode: 'upload-from',
                    id: good.id
                  })}`
                })
              })}
            >
              <View className="at-icon at-icon-lock footer__icon" />
              上架新的
            </View>
          )}
          {statusButton === 'remove' && (
            <View
              className="footer__status"
              onClick={() => {
                if (good.status === GoodStatus.ForSell)
                  showModal({
                    title: '确认下架物品',
                    message: `您真的要下架商品 "${good.name}" 吗？\r\n\r\n您之后可以在"我的">"物品管理">"下架物品"中重新编辑上架`,
                    onClose: 0,
                    buttons: [
                      ['再想想', () => {}],
                      [
                        '确认下架',
                        statusStabilizer(async () => {
                          await updateGood({
                            id: good.id,
                            status: GoodStatus.NotAvailable
                          })
                          await revalidate()
                        }),
                        'color:#e30909'
                      ]
                    ]
                  })
              }}
            >
              <View className="at-icon at-icon-lock footer__icon" />
              下架
            </View>
          )}
        </Block>
      )}
    </View>
  )
}

SellerFooter.defaultProps = {}

export default SellerFooter
