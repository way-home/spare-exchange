import Taro, { FC, useMemo, useContext } from '@tarojs/taro'
import { View, Button, Block } from '@tarojs/components'
import {
  loginModalConfig,
  infoModalConfig,
  resolveBoolean
} from '@/components/authModal'
import ModalContext from '../modal'

import {
  // Good
  Good,
  GoodStatus,
  // Star
  GoodStar,
  getGoodStarsAPI,
  createGoodStar,
  deleteGoodStar,
  // User
  hasCompleteInfo,
  // Order
  Order,
  OrderStatus,
  getOrdersAPI,
  createOrder
} from '@/models'
import useSWR from 'swr'

import { useUserStore, LoginStatus } from '@/store'

import { echo, useStabilizer, encodeParams } from '@/utils'

import './index.scss'

interface FooterProps {
  good?: Good
  revalidate: () => Promise<boolean>
}

const BuyerFooter: FC<FooterProps> = ({ good }) => {
  const showModal = useContext(ModalContext)
  // user info
  const [{ loginStatus, id: me, ...restInfo }, userActions] = useUserStore()
  const authorized = loginStatus === LoginStatus.Authorized
  const logging = loginStatus === LoginStatus.Pending
  const notAuthorized = loginStatus === LoginStatus.Failed

  // star data
  const { data: stars, mutate: mutateStar, revalidate: fetchStar } = useSWR<
    GoodStar[]
  >(authorized ? getGoodStarsAPI() : null)
  const star = useMemo(
    () =>
      good != null && stars != null
        ? stars.find(({ good: { id } }) => id === good.id)
        : null,
    [good, stars]
  )
  // for some bullshit reason this flag can only computed outside JSX
  // freak Taro
  const starNotExisted = star == null

  // order data
  const { data: orders, revalidate: fetchOrders } = useSWR<Order[]>(
    authorized && good != null ? getOrdersAPI({ good: good.id }) : null
  )
  const order = useMemo(
    () =>
      good != null && orders != null
        ? orders.find(
            ({ status, good: id, buyer }) =>
              status === OrderStatus.OnGoing && id === good.id && buyer === me
          )
        : null,
    [good, orders, me]
  )
  const orderNotExist = order == null

  // stabilizer
  const starStabilizer = useStabilizer(300)
  const buyStabilizer = useStabilizer(300)

  // modals
  const showAuthModal = () =>
    new Promise<boolean>(r =>
      showModal(loginModalConfig(userActions, resolveBoolean(r)))
    )
  const showInfoModal = () =>
    new Promise<boolean>(r =>
      showModal({
        ...infoModalConfig(resolveBoolean(r)),
        message: '\r\n该功能需要您填写一些个人信息才能使用，现在就去填写吗'
      })
    )
  const showInfoLoginModal = () =>
    new Promise<boolean>(r => showModal(infoModalConfig(resolveBoolean(r))))

  return (
    <View className="footer">
      <Button className="footer__share" openType="share">
        <View className="at-icon at-icon-share footer__icon" />
        分享
      </Button>
      {good && good.status === GoodStatus.ForSell && (
        <Block>
          {starNotExisted ? (
            <View
              className="footer__star"
              onClick={starStabilizer(async () => {
                if (authorized) {
                  await createGoodStar(good.id)
                  await fetchStar()
                }
                if (notAuthorized) {
                  if (!(await showAuthModal())) return
                }
              })}
            >
              <View className="at-icon at-icon-star footer__icon" />
              收藏
            </View>
          ) : (
            // the button should only show when authorized
            <View
              className="footer__star"
              onClick={starStabilizer(async () => {
                await deleteGoodStar(star!.id)
                if (stars != null)
                  await mutateStar(
                    echo(stars.filter(gs => gs.id !== star!.id)),
                    false
                  )
              })}
            >
              <View className="at-icon at-icon-star-2 footer__icon footer__star--active" />
              取消收藏
            </View>
          )}
          {orderNotExist ? (
            <View
              className="footer__buy"
              onClick={async () => {
                if (authorized) {
                  if (hasCompleteInfo(restInfo)) {
                    const confirm = await new Promise(r =>
                      showModal({
                        title: '确认创建订单',
                        message:
                          '确认后将为您创建一笔待确认订单，您可以在“订单管理”中取消订单\r\n\r\n' +
                          '同时我们将会为您提供卖家的联系方式等信息以便后续沟通交易，确定创建订单么？',
                        onClose: 0,
                        buttons: [
                          ['再看看', buyStabilizer(() => r(false))],
                          ['确认并查看信息', buyStabilizer(() => r(true))]
                        ]
                      })
                    )
                    if (confirm) {
                      await createOrder(good.id)
                      await fetchOrders()
                      await Taro.navigateTo({
                        url: `/pages/userInfo/index${encodeParams({
                          mode: 'contact',
                          id: good.owner
                        })}`
                      })
                    }
                  } else {
                    await showInfoModal()
                  }
                }
                if (notAuthorized) {
                  if (!(await showAuthModal())) return
                  if (!hasCompleteInfo(restInfo)) {
                    await showInfoLoginModal()
                  }
                }
              }}
            >
              <View className="at-icon at-icon-shopping-bag footer__icon" />
              想买
            </View>
          ) : (
            <View
              className="footer__contact"
              onClick={buyStabilizer(async () => {
                await Taro.navigateTo({
                  url: `/pages/userInfo/index${encodeParams({
                    mode: 'contact',
                    id: good.owner
                  })}`
                })
              })}
            >
              <View className="at-icon at-icon-user footer__icon" />
              联系卖家
            </View>
          )}
        </Block>
      )}
    </View>
  )
}

BuyerFooter.defaultProps = {}

export default BuyerFooter
