import Taro from '@tarojs/taro'
import { ShowModalParams } from '@/components/modal/useModalState'

const ModalContext = Taro.createContext<(config: ShowModalParams) => void>(
  _ => {}
)

export default ModalContext
