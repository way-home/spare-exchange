import Taro, {
  FC,
  usePullDownRefresh,
  useRouter,
  useEffect,
  useMemo,
  useShareAppMessage
} from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'
import { AtActivityIndicator } from 'taro-ui'
import { GoodCard, UserCard, Divider } from '@/components'

import {
  Good,
  BasicUser,
  getGoodsAPI,
  getUserInfoAPI,
  GoodStatus
} from '@/models'
import useSWR from 'swr'

import { encodeParams } from '@/utils'

import './index.scss'

import { useUserStore } from '@/store'

const UserHomePage: FC = () => {
  const { params } = useRouter()
  const id: number = parseInt(params.id)

  const [{ id: myId }] = useUserStore()
  const { data: user, revalidate: fetchUser } = useSWR<BasicUser>(
    getUserInfoAPI(id)
  )
  const { data: allGoods, error: goodErr, revalidate: fetchGoods } = useSWR<
    Good[]
  >(getGoodsAPI({ owner: id }))

  const isMyHome = myId != null && id === myId
  const title = user ? `${user.wx_info.nickName}的小店` : null
  const topTitle = isMyHome ? '我的小店' : title

  const goods = useMemo(
    () =>
      allGoods == null
        ? allGoods
        : allGoods.filter(g => g.status === GoodStatus.ForSell),
    [allGoods, isMyHome]
  )

  useEffect(() => {
    console.log('isMyHome', isMyHome)
  }, [isMyHome])

  useEffect(() => {
    if (topTitle != null) Taro.setNavigationBarTitle({ title: topTitle })
  }, [topTitle])

  usePullDownRefresh(() => {
    Taro.showNavigationBarLoading()
    Promise.all([fetchUser(), fetchGoods()]).then(() => {
      Taro.hideNavigationBarLoading()
      Taro.stopPullDownRefresh()
    })
  })

  useShareAppMessage(() => {
    if (title != null)
      return {
        title: `蛋壳 - ${title}`,
        path: '/pages/home/index'
      }
  })

  return (
    <View className="user-home-page">
      <UserCard user={user} size="large" contentLevel="detailed" />
      <Divider top bottom />
      <View className="good-list">
        <View className="good-list__title">上架商品</View>
        {!!goods &&
          !goodErr &&
          goods.map(good => (
            <View
              key={good.id}
              className="good-list__item"
              onClick={() =>
                Taro.navigateTo({
                  url: `/pages/goodDetail/index${encodeParams({
                    id: good.id
                  })}`
                })
              }
            >
              <GoodCard
                good={good}
                contentLevel={isMyHome ? 'minimal' : 'simple'}
              >
                {isMyHome && (
                  <View
                    className="good-actions"
                    onClick={e => e.stopPropagation()}
                  >
                    <Button className="good-action">修改商品</Button>
                    <Button className="good-action">下架商品</Button>
                  </View>
                )}
              </GoodCard>
            </View>
          ))}
        {!goods && !goodErr && (
          <View className="good-list__loading">
            <AtActivityIndicator />
            <Text className="good-list__loading-text">加载中...</Text>
          </View>
        )}
      </View>
    </View>
  )
}

UserHomePage.config = {
  navigationBarTitleText: '',
  enablePullDownRefresh: true
}

export default UserHomePage
