import Taro, { FC, useState, useMemo } from '@tarojs/taro'
import { View, Image, Button } from '@tarojs/components'
import { AtSegmentedControl } from 'taro-ui'
import { OrderCard } from '@/components'

import {
  Good,
  Order,
  cancelOrder,
  OrderStatus,
  isCancelled,
  getOrdersAPI
} from '@/models'

import useSWR from 'swr'
import { encodeParams, deduplicate, useStabilizer } from '@/utils'

import { TabType } from './params'
import { useUserStore } from '@/store'

import homeIcon from '@/assets/img/tabbar/home.png'
import sellIcon from '@/assets/img/tabbar/sell.png'

import './index.scss'

const gotoGoodPage = (id: Good['id'], dist?: 'top' | 'orders') =>
  Taro.navigateTo({
    url: `/pages/goodDetail/index${encodeParams({ id, dist })}`
  })

const OngoingOrders: FC = () => {
  const [current, setCurrent] = useState(TabType.ShoppingList)
  const [{ id: me }] = useUserStore()
  const { data: orders, mutate: refetch } = useSWR<Order[]>(getOrdersAPI())
  const shoppingList = useMemo(
    () =>
      orders &&
      [...orders]
        .filter(o => o.buyer === me)
        .filter(o =>
          [OrderStatus.Success, OrderStatus.Unknown].every(s => s !== o.status)
        )
        /* sort ongoing orders on top */
        .sort((a, b) => a.status - b.status),
    [orders]
  )
  const goodList = useMemo(
    () =>
      orders &&
      deduplicate(
        orders
          .filter(o => o.status === OrderStatus.OnGoing)
          .filter(o => o.seller === me),
        o => o.good
      ),
    [orders]
  )
  const isShoppingList = current === TabType.ShoppingList
  const list = isShoppingList ? shoppingList : goodList

  const actionStabilizer = useStabilizer(300)

  const bottomStabilizer = useStabilizer(300)

  // TODO: optional, add pulling down to refresh feature in this page
  return (
    <View className="ongoing-orders-page">
      <View className="ongoing-orders-page__tab-bar-wrapper">
        <View className="ongoing-orders-page__tab-bar">
          <AtSegmentedControl
            values={['我要买的', '我要卖的']}
            current={current}
            onClick={c => setCurrent(c)}
          />
        </View>
      </View>
      <View className="ongoing-orders-page__orders-list">
        {list &&
          list.map(order => (
            <View
              key={order.id}
              className="ongoing-orders-page__orders-list-item"
              onClick={() => gotoGoodPage(order.good)}
            >
              <OrderCard order={order} showStatus={isCancelled(order.status)}>
                {!isCancelled(order.status) && (
                  <View
                    className="actions-container"
                    onClick={e => e.stopPropagation()}
                  >
                    <Button
                      className="action-widget"
                      onClick={actionStabilizer(
                        isShoppingList
                          ? async () => {
                              await cancelOrder(order.id)
                              await refetch()
                            }
                          : () => {
                              gotoGoodPage(order.good, 'orders')
                            }
                      )}
                    >
                      {isShoppingList ? '取消订单' : '管理订单'}
                    </Button>
                  </View>
                )}
              </OrderCard>
            </View>
          ))}
        {list != null &&
          (current === TabType.ShoppingList ? (
            <View className="no-more-orders">
              似乎没有更多订单了，去
              <View
                className="no-more-orders__action"
                onClick={bottomStabilizer(() => {
                  Taro.switchTab({ url: '/pages/home/index' })
                })}
              >
                <Image className="no-more-orders__icon" src={homeIcon} />
                首页
              </View>
              逛逛，看看有没有什么心仪的商品吧！
            </View>
          ) : (
            <View className="no-more-orders">
              似乎没有更多订单了，您可以
              <View
                className="no-more-orders__action"
                onClick={bottomStabilizer(() => {
                  Taro.switchTab({ url: '/pages/sell/index' })
                })}
              >
                <Image className="no-more-orders__icon" src={sellIcon} />
                上传新的物品
              </View>
              {list.length > 0
                ? '，或者分享物品和小店的二维码，让更多人看到'
                : ''}
            </View>
          ))}
      </View>
    </View>
  )
}

OngoingOrders.config = {
  navigationBarTitleText: '待确认订单管理'
}

export default OngoingOrders
