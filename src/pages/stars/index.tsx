import Taro, {
  FC,
  useState,
  usePullDownRefresh,
  useEffect,
  useDidShow
} from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import { AtSegmentedControl, AtSwipeAction } from 'taro-ui'
import { GoodCard } from '@/components'

import { encodeParams, useStabilizer } from '@/utils'

import { TabType } from './params'

import { GoodStar, getGoodStarsAPI, deleteGoodStar } from '@/models'
import useSWR from 'swr'

import { useUserStore, LoginStatus } from '@/store'

import './index.scss'

import homeIcon from '@/assets/img/tabbar/home.png'

const StarsPage: FC = () => {
  const [current, setCurrent] = useState(TabType.Available)

  const [{ loginStatus }] = useUserStore()
  const authorized = loginStatus === LoginStatus.Authorized

  const {
    data: starGoods,
    error: starGoodErr,
    revalidate: fetchStarGood,
    mutate: refresh
  } = useSWR<GoodStar[]>(
    authorized
      ? getGoodStarsAPI({
          status: current === TabType.Available ? 'valid' : 'invalid'
        })
      : null
  )

  useDidShow(() => {
    refresh()
  })

  usePullDownRefresh(() => {
    Taro.showNavigationBarLoading()
    fetchStarGood().then(() => {
      Taro.hideNavigationBarLoading()
      Taro.stopPullDownRefresh()
    })
  })

  useEffect(() => {
    if (starGoodErr != null) {
      console.error(starGoodErr)
      Taro.showToast({
        title: `加载失败`,
        icon: 'none',
        duration: 2000
      })
    }
  }, [starGoodErr])

  const navigateStabilizer = useStabilizer(300)
  const deleteStabilizer = useStabilizer(300)
  const homeStabilizer = useStabilizer(300)

  // TODO: optional, add pulling down indicator in this page
  return (
    <View className="stars-page">
      <View className="stars-page__tab-bar-wrapper">
        <View className="stars-page__tab-bar">
          <AtSegmentedControl
            values={['收藏物品', '失效物品']}
            current={current}
            onClick={c => setCurrent(c)}
          />
        </View>
      </View>
      <View className="stars-page__stars-list">
        {!!starGoods &&
          starGoods.map(starGood => (
            <View key={starGood.id} className="stars-page__stars-list-item">
              <AtSwipeAction
                options={[
                  {
                    text: '删除收藏',
                    style: {
                      backgroundColor: '#FF4949'
                    }
                  }
                ]}
                onClick={deleteStabilizer(async () => {
                  await deleteGoodStar(starGood.id)
                  await refresh(
                    starGoods.filter(gs => gs.id !== starGood.id),
                    false
                  )
                })}
              >
                <View className="stars-page__stars-list-swipe">
                  <GoodCard
                    good={starGood.good}
                    showPrice={current !== TabType.NotAvailable}
                    onClick={navigateStabilizer(() => {
                      if (current !== TabType.NotAvailable)
                        Taro.navigateTo({
                          url: `/pages/goodDetail/index${encodeParams({
                            id: starGood.good.id
                          })}`
                        })
                    })}
                  />
                </View>
              </AtSwipeAction>
            </View>
          ))}
        {starGoods != null &&
          (current === TabType.Available ? (
            <View className="no-more-stars">
              似乎没有更多收藏了，去
              <View
                className="no-more-stars__home"
                onClick={homeStabilizer(() => {
                  Taro.switchTab({ url: '/pages/home/index' })
                })}
              >
                <Image className="no-more-stars__icon" src={homeIcon} />
                首页
              </View>
              逛逛，看看有没有什么心仪的商品吧！
            </View>
          ) : (
            <View className="no-more-stars">没有更多了</View>
          ))}
      </View>
    </View>
  )
}

StarsPage.config = {
  navigationBarTitleText: '收藏',
  enablePullDownRefresh: true
}

export default StarsPage
