import Taro, { FC } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { AtButton, AtTimeline } from 'taro-ui'
import { Modal } from '@/components'
import { useModalState, useShowModal } from '@/components/modal/useModalState'
import {
  loginModalConfig,
  infoModalConfig,
  resolveBoolean
} from '@/components/authModal'

import { hasCompleteInfo } from '@/models'

import { useUserStore, LoginStatus } from '@/store'

import './index.scss'

const SellPage: FC = () => {
  const [user, userActions] = useUserStore()
  const authorized = user.loginStatus === LoginStatus.Authorized
  const notAuthorized = user.loginStatus === LoginStatus.Failed
  // modals state
  const [modalState, modalActions] = useModalState()
  const showModal = useShowModal(modalActions)

  // modals
  // hide tab bar when modal showing
  const tabBarControl = {
    onEnter: () => Taro.hideTabBar({}),
    onLeave: () => Taro.showTabBar({})
  }
  const showAuthModal = () =>
    new Promise<boolean>(r =>
      showModal({
        ...loginModalConfig(userActions, resolveBoolean(r)),
        ...tabBarControl
      })
    )
  const showInfoModal = () =>
    new Promise<boolean>(r =>
      showModal({
        ...infoModalConfig(resolveBoolean(r)),
        ...tabBarControl,
        message: '\r\n该功能需要您填写一些个人信息才能使用，现在就去填写吗'
      })
    )
  const showInfoLoginModal = () =>
    new Promise<boolean>(r =>
      showModal({ ...infoModalConfig(resolveBoolean(r)), ...tabBarControl })
    )

  return (
    <View className="sell-page">
      <View className="sell-page__explain">
        <AtTimeline
          className="sell-page__time-line"
          items={[
            { title: '扫描书籍条码录入 或 填写信息发布闲置物品' },
            // { title: '生成个人小店二维码 或 商品二维码保存到相册' },
            { title: '点击 小店 或 商品 的分享按钮' },
            { title: '可自行分享到群、朋友圈等地方吸引购买' }
          ]}
        />
      </View>
      <View className="sell-page__upload">
        <AtButton
          onClick={async () => {
            if (authorized) {
              if (hasCompleteInfo(user)) {
                Taro.navigateTo({
                  url: '/pages/goodForm/index?mode=upload-scan'
                })
              } else {
                await showInfoModal()
              }
            }
            if (notAuthorized) {
              if (!(await showAuthModal())) return
              if (!hasCompleteInfo(user)) {
                await showInfoLoginModal()
              }
            }
          }}
        >
          扫描条码上传
        </AtButton>
        <AtButton
          onClick={async () => {
            if (authorized) {
              if (hasCompleteInfo(user)) {
                Taro.navigateTo({ url: '/pages/goodForm/index?mode=upload' })
              } else {
                await showInfoModal()
              }
            }
            if (notAuthorized) {
              if (!(await showAuthModal())) return
              if (!hasCompleteInfo(user)) {
                await showInfoLoginModal()
              }
            }
          }}
        >
          直接填写信息
        </AtButton>
      </View>
      <Modal state={modalState} />
    </View>
  )
}

SellPage.config = {
  navigationBarTitleText: '发布闲置'
}

export default SellPage
