import Taro, {
  FC,
  useRouter,
  useEffect,
  useState,
  useMemo,
  useDidShow
} from '@tarojs/taro'
import { View, Block, Image } from '@tarojs/components'
import { AtSegmentedControl, AtButton } from 'taro-ui'
import { GoodCard } from '@/components'

import { getGoodsAPI, Good, GoodStatus, updateGood } from '@/models'
import useSWR from 'swr'

import { useUserStore, LoginStatus } from '@/store'

import { TabType } from './params'

import { encodeParams, useStabilizer } from '@/utils'

import sellIcon from '@/assets/img/tabbar/sell.png'

import './index.scss'

const MyGoods: FC = () => {
  const { params } = useRouter()
  const [current, setCurrent] = useState(TabType.ForSell)
  useEffect(() => {
    params.tab && setCurrent(parseInt(params.tab) as TabType)
  }, [])

  const [{ id: me, loginStatus }] = useUserStore()
  const authorized = loginStatus === LoginStatus.Authorized

  const selectedStatus = useMemo(
    () =>
      (({
        [TabType.ForSell]: GoodStatus.ForSell,
        [TabType.NotAvailable]: GoodStatus.NotAvailable,
        [TabType.Sold]: GoodStatus.Sold
      } as Record<TabType, GoodStatus>)[current]),
    [current]
  )

  const { data: allMyGoods, revalidate: fetchGoods } = useSWR<Good[]>(
    authorized ? getGoodsAPI({ owner: me }) : null
  )
  useDidShow(() => {
    // revalidate goods when page shows up
    fetchGoods()
  })
  const goods = useMemo(
    () =>
      allMyGoods == null
        ? allMyGoods
        : allMyGoods.filter(good => good.status === selectedStatus),
    [allMyGoods, selectedStatus]
  )

  const bottomStabilizer = useStabilizer(300)

  // TODO: optional, add pulling down to refresh feature in this page
  return (
    <View className="my-goods-page">
      <View className="my-goods-page__tab-bar-wrapper">
        <View className="my-goods-page__tab-bar">
          <AtSegmentedControl
            values={['我上架的', '我卖出的', '下架物品']}
            current={current}
            onClick={c => setCurrent(c)}
          />
        </View>
      </View>
      <View className="my-goods-page__goods-list">
        {goods != null &&
          goods.map(good => (
            <View
              key={good.id}
              className="my-goods-page__goods-list-item"
              onClick={() =>
                Taro.navigateTo({
                  url: `/pages/goodDetail/index${encodeParams({ id: good.id })}`
                })
              }
            >
              <GoodCard good={good} contentLevel="minimal">
                <View
                  className="good-actions"
                  onClick={e => e.stopPropagation()}
                >
                  {current === TabType.ForSell && (
                    <Block>
                      {/* <AtButton
                        className="good-action"
                        onClick={() => {
                          console.log('生成二维码') // TODO: generate QR code
                        }}
                      >
                        生成二维码
                      </AtButton> */}
                      <AtButton
                        className="good-action"
                        customStyle="width: 60px" // work around
                        onClick={async () => {
                          await updateGood({
                            id: good.id,
                            status: GoodStatus.NotAvailable
                          })
                          await fetchGoods()
                        }}
                      >
                        下架
                      </AtButton>
                      <AtButton
                        className="good-action"
                        customStyle="width: 60px" // work around
                        onClick={() => {
                          Taro.navigateTo({
                            url: `/pages/goodForm/index${encodeParams({
                              mode: 'edit',
                              id: good.id
                            })}`
                          })
                        }}
                      >
                        修改
                      </AtButton>
                    </Block>
                  )}
                  {current === TabType.Sold && (
                    <AtButton
                      className="good-action"
                      onClick={() =>
                        Taro.navigateTo({
                          url: `/pages/goodForm/index${encodeParams({
                            mode: 'upload-from',
                            id: good.id
                          })}`
                        })
                      }
                    >
                      修改信息后上架新物品
                    </AtButton>
                  )}
                  {current === TabType.NotAvailable && (
                    <Block>
                      <AtButton
                        className="good-action"
                        onClick={async () => {
                          await updateGood({
                            id: good.id,
                            status: GoodStatus.ForSell
                          })
                          await fetchGoods()
                        }}
                      >
                        重新上架
                      </AtButton>
                      <AtButton
                        className="good-action"
                        onClick={() => {
                          Taro.navigateTo({
                            url: `/pages/goodForm/index${encodeParams({
                              mode: 'edit-available',
                              id: good.id
                            })}`
                          })
                        }}
                      >
                        修改信息后重新上架
                      </AtButton>
                    </Block>
                  )}
                </View>
              </GoodCard>
            </View>
          ))}
        {goods != null && (
          <View className="no-more-goods">
            似乎没有更多订单了，您可以
            <View
              className="no-more-goods__action"
              onClick={bottomStabilizer(() => {
                Taro.switchTab({ url: '/pages/sell/index' })
              })}
            >
              <Image className="no-more-goods__icon" src={sellIcon} />
              上传新的物品
            </View>
            {current === TabType.ForSell && goods.length > 0
              ? '或者分享物品和小店，让更多人看到'
              : ''}
            {/* '或者分享物品和小店的二维码，让更多人看到' */}
          </View>
        )}
      </View>
    </View>
  )
}

MyGoods.config = {
  navigationBarTitleText: '物品管理'
}

export default MyGoods
