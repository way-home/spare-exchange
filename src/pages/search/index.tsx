import Taro, { FC, useEffect, useState, useMemo } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtSearchBar, AtActivityIndicator } from 'taro-ui'
import { GoodCard } from '@/components'

import classNames from 'classnames'

import { Good, GoodStatus, getGoodsAPI } from '@/models'
import useSWR from 'swr'

import { encodeParams } from '@/utils'

import './index.scss'

type SortModeName = 'none' | 'priceIncrease' | 'priceDecrease'

type SortKey = {
  idx: number
  current_price: number
}

type SortMode = {
  name: string
  fn: ((a: SortKey, b: SortKey) => number) | null
}

const sortOptions: Record<SortModeName, SortMode> = {
  none: {
    name: '综合排序',
    fn: null
  },
  priceIncrease: {
    name: '价格从低到高',
    fn: (a, b) => a.current_price - b.current_price
  },
  priceDecrease: {
    name: '价格从高到低',
    fn: (a, b) => b.current_price - a.current_price
  }
}

const SearchPage: FC = () => {
  const { data: allGoods, error: goodErr } = useSWR<Good[]>(getGoodsAPI())

  const [inputKeyword, setInputKeyword] = useState('')
  const [keyword, setKeyword] = useState(inputKeyword)
  const [sortMode, setSortMode] = useState<SortModeName>('none')
  const sortFn = useMemo(() => sortOptions[sortMode].fn, [sortMode])

  const allGoodsForSell = useMemo(
    () =>
      allGoods == null
        ? allGoods
        : allGoods.filter(g => g.status === GoodStatus.ForSell),
    [allGoods]
  )
  // search
  const matchedGoods = useMemo(
    () =>
      allGoodsForSell == null || keyword === ''
        ? allGoodsForSell
        : allGoodsForSell.filter(g => g.name.includes(keyword)),
    [keyword, allGoodsForSell]
  )
  // sort
  const sortKeys = useMemo(() => {
    if (matchedGoods == null) return null
    const keys = matchedGoods.map(({ current_price }, idx) => ({
      idx,
      current_price: parseFloat(current_price)
    }))
    return sortFn != null ? keys.sort(sortFn) : keys
  }, [matchedGoods, sortFn])
  const goods = useMemo(
    () =>
      matchedGoods == null || sortKeys == null
        ? matchedGoods
        : sortKeys.map(({ idx }) => matchedGoods[idx]),
    [matchedGoods, sortKeys]
  )

  useEffect(() => {
    if (goodErr)
      Taro.showToast({
        title: `加载失败`,
        icon: 'none',
        duration: 2000
      })
  }, [goodErr])

  return (
    <View className="index-page">
      <View className="page-content">
        <AtSearchBar
          actionName="搜索"
          value={keyword}
          onChange={k => setInputKeyword(k)}
          onActionClick={() => setKeyword(inputKeyword)}
          onConfirm={() => setKeyword(inputKeyword)}
          onClear={() => {
            setInputKeyword('')
            setKeyword('')
          }}
          focus
        />
        <View className="sort-panel">
          {Object.entries(sortOptions).map(option => {
            const [mode, { name }] = option
            return (
              <View
                key={mode}
                className={classNames('sort-panel__option', {
                  'sort-panel__option--active': mode === sortMode
                })}
                onClick={() => {
                  setSortMode(mode as SortModeName)
                }}
              >
                {name}
              </View>
            )
          })}
        </View>
        <View className="item-list">
          {!!goods &&
            !goodErr &&
            goods.map(good => (
              <View
                key={good.id}
                className="item-list__item"
                onClick={() =>
                  Taro.navigateTo({
                    url: `/pages/goodDetail/index${encodeParams({
                      id: good.id
                    })}`
                  })
                }
              >
                <GoodCard good={good} />
              </View>
            ))}
          {!goods && !goodErr && (
            <View className="item-list__loading">
              <AtActivityIndicator />
              <Text className="item-list__loading-text">加载中...</Text>
            </View>
          )}
        </View>
      </View>
    </View>
  )
}

SearchPage.config = {
  navigationBarTitleText: '搜索物品'
}

export default SearchPage
