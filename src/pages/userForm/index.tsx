import Taro, {
  FC,
  useState,
  useEffect,
  useMemo,
  useCallback
} from '@tarojs/taro'
import { View, Picker, Text, Button } from '@tarojs/components'
import {
  AtInput,
  AtImagePicker,
  AtButton,
  AtMessage,
  AtModal,
  AtModalHeader,
  AtModalContent,
  AtModalAction
} from 'taro-ui'
import { CustomInput } from '@/components'

import { WxInfo, updateMyInfo, UserInfo, updateWxImage } from '@/models'

import {
  useForm,
  fallbackAdapter,
  GenericComponentAdapter as GCA,
  yupSchema,
  fileInfo,
  telRegex,
  PartialNullable,
  diffObject,
  filterProps,
  echo
} from '@/utils'
import { string as yupString, object as yupObject, InferType } from 'yup'

import { useUserStore, LoginStatus } from '@/store/user'

import rawCollegeData from '@/assets/json/college.json'

import './index.scss'

// work around for the bad design of type declaration of taro-ui
// import { ComponentClass } from 'react'

// type ComponentClassProps<Comp> = Comp extends ComponentClass<infer T>
//   ? T
//   : never
// type AtImagePickerProps = ComponentClassProps<typeof AtImagePicker>
// type File = AtImagePickerProps['files'][0]

const collegeData = Object.entries(rawCollegeData)
  .map(p => p[1])
  .reduce((d, l) => d.concat(l), [])

const sendErrMsg = (message: string) => {
  if (message !== '')
    Taro.atMessage({
      message,
      type: 'error',
      duration: 1500
    })
}

const validateSchema = yupObject().shape({
  number: yupString().required('学号不能空缺'),
  name: yupString().required('姓名不能空缺'),
  college: yupString().required('学院不能空缺'),
  email: yupString().email('请输入正确的邮箱'),
  phone: yupString().matches(telRegex, {
    message: '请输入正确的电话号码',
    excludeEmptyString: true
  }),
  wx_num: yupString(),
  wx_image: fileInfo.nullable().notRequired()
})

type FormType = InferType<typeof validateSchema>

const initFormData: FormType = {
  name: '',
  number: '',
  phone: '',
  email: '',
  college: '',
  wx_image: undefined,
  wx_num: ''
}

const editOnceDataLabel = {
  name: '姓名',
  college: '学院',
  number: '学号'
}

const UserFormPage: FC = () => {
  const [
    {
      wx_info: { wx_image, wx_num },
      user_info: { name, college, number, email, phone }
    },
    { updateBackend, updateWechat }
  ] = useUserStore()
  const originalUserData = {
    name,
    number,
    college,
    wx_image,
    wx_num,
    email,
    phone
  }

  // test if field is not editable
  const editOnceData = { name, college, number }
  const editOnceDataName = useMemo(
    () => Object.keys(editOnceData),
    Object.values(editOnceData)
  )
  const noEditDataName = useMemo(
    () =>
      Object.entries(editOnceData)
        .filter(([_, v]) => v != null && v !== '')
        .map(([k, _]) => k),
    Object.values(editOnceData)
  )
  const isNoEditData = useCallback(
    (name: string) => noEditDataName.includes(name),
    noEditDataName
  )

  // modal state
  const [confirmFlag, setModalFlag] = useState(false)
  const [modalMsg, setModalMsg] = useState<[string, string][]>([])
  const [modalConfirm, setModalConfirm] = useState<() => void>(() => {})
  const [modalCancel, setModalCancel] = useState<() => void>(() => {})
  // modal method
  const showModal = () =>
    new Promise<boolean>((resolve, _) => {
      setModalConfirm(() => () => {
        resolve(true)
        setModalFlag(false)
      })
      setModalCancel(() => () => {
        resolve(false)
        setModalFlag(false)
      })
      setModalFlag(true)
    })

  // construct adapter
  const commonAdapter = useCallback<GCA<keyof FormType, FormType>>(
    configs => {
      const { error, name, formDisabled } = configs
      return {
        ...fallbackAdapter(configs),
        error: error != null,
        disabled: formDisabled || isNoEditData(name)
      }
    },
    [isNoEditData]
  )
  const customInputAdapter = useCallback<GCA<keyof FormType, FormType>>(
    ({ name, formDisabled, error }) => {
      return {
        error: error != null,
        disabled: formDisabled || isNoEditData(name)
      }
    },
    [isNoEditData]
  )

  // use form
  const {
    setValues,
    register,
    values,
    handleSubmit,
    formDisabled,
    setValueAndValidate,
    validate
  } = useForm({
    initialData: initFormData,
    onError: errs => sendErrMsg(Object.values(errs)[0] as string),
    validateSchema: yupSchema(validateSchema),
    defaultAdapter: commonAdapter,
    validateBeforeSubmit: false,
    onSubmit: async ({ values, setErrors, onError, validate }) => {
      const { wx_image, ...rest } = values
      const { phone, email, wx_num } = rest

      /* 
      manually validate. have to do this here because `yup.mixed.when` is
      somehow broken in Taro 
      */

      let errOccurred: boolean = false
      try {
        await validate()
      } catch (err) {
        errOccurred = true
      }
      // validate contact method
      const contactMethods = { phone, email, wx_num, wx_image }
      if (Object.values(contactMethods).every(t => t == null || t === '')) {
        const msg = '至少应填写一种联系方式，以便您与其他用户相互联系'
        let err = {}
        // for some mysterious reasons, `for (const ... of ...) ...` can't be used here
        let k
        for (k of Object.keys(contactMethods)) {
          err[k] = msg
        }
        setErrors(err)
        if (!errOccurred) {
          errOccurred = true
          onError(err)
        }
      }
      if (errOccurred) return

      // make payload
      type PayLoad = PartialNullable<WxInfo & UserInfo>
      const rawPayload: PayLoad = { ...rest }
      if (wx_image != null) rawPayload.wx_image = wx_image.url
      let { wx_image: newWxImage, ...restPayload }: PayLoad = diffObject<
        PayLoad,
        null
      >(originalUserData, rawPayload, null, v => v == null || v === '')
      let payload: PayLoad = restPayload
      if (newWxImage === null) {
        payload.wx_image = null
      }

      // show confirm modal
      let modalMsg: [string, string][] = []
      for (const k of editOnceDataName) {
        const v = payload[k]
        if (v != null && v !== '') {
          modalMsg.push([editOnceDataLabel[k], v])
        }
      }
      if (modalMsg.length > 0) {
        setModalMsg(modalMsg)
        const flag = await showModal()
        if (!flag) return
      }

      try {
        // note the upload order below can not be reserved, or error might occur in backend
        if (Object.keys(payload).length > 0)
          updateBackend(await updateMyInfo(payload))
        if (newWxImage != null) {
          const {
            wx_info: { wx_image }
          } = await updateWxImage(newWxImage)
          updateWechat({ wx_image })
        }
        Taro.navigateBack()
      } catch (err) {
        // error occurred on network transfer or remote
        console.error(err)
        if (err.response != null) {
          const { data, status } = err.response
          if (status === 400 && data != null) {
            // notify backend error
            setErrors(data)
            onError(data)
          } else {
            sendErrMsg(`发生错误：${status}`)
          }
        } else {
          sendErrMsg(`发生错误：${err.message}`)
        }
      }
    }
  })

  // auto fill form with old user data
  useEffect(() => {
    const { wx_image, ...rest } = originalUserData
    let orgValues: Partial<FormType> = {}
    if (wx_image != null)
      orgValues.wx_image = { url: wx_image, file: undefined }
    for (const [k, v] of Object.entries(rest)) {
      if (v != null) orgValues[k] = v
    }
    setValues(vs => ({ ...vs, ...orgValues }))
  }, Object.values(originalUserData))

  const collageComp = values.college ? (
    <Text>{values.college}</Text>
  ) : (
    <Text style="color:#808080">请选择学院</Text>
  )

  return (
    <View className="user-form">
      <AtMessage />
      <AtInput
        {...register('number')}
        required
        title="学号"
        type="text"
        placeholder="请输入学号"
      />
      <AtInput
        {...register('name')}
        required
        title="姓名"
        type="text"
        placeholder="请输入姓名"
      />
      <CustomInput
        {...register('college', customInputAdapter)}
        required
        label="学院"
      >
        {!register('college', customInputAdapter).disabled ? (
          <Picker
            onCancel={() => validate('college').catch(() => {})}
            mode="selector"
            value={
              values.college
                ? collegeData.findIndex(n => n === values.college)
                : 0
            }
            onChange={e =>
              setValueAndValidate('college', collegeData[e.detail.value])
            }
            range={collegeData}
          >
            {collageComp}
          </Picker>
        ) : (
          collageComp
        )}
      </CustomInput>
      <AtInput
        {...register('email')}
        type="text"
        title="邮箱"
        placeholder="请输入邮箱"
      />
      <AtInput
        {...register('phone')}
        title="电话"
        type="text"
        placeholder="请输入电话"
      />
      <AtInput
        {...register('wx_num')}
        title="微信号"
        type="text"
        placeholder="请输入微信号"
      />
      <CustomInput
        label="微信二维码"
        labelPosition="top"
        labelStyle="width:160rpx; padding-top:10rpx; margin-right:10rpx"
        iconPosition="top"
        iconStyle="padding-top:15rpx;"
        {...register('wx_image', customInputAdapter)}
      >
        <AtImagePicker
          showAddBtn
          count={1}
          onFail={e => console.error(e)}
          {...register('wx_image', ({ formDisabled, value, setValue }) => ({
            showAddBtn: !(formDisabled || value != null),
            files: value != null ? [value] : [],
            onChange: newImages => {
              // freak the bullshit between `props?: T` and `props: T | undefined`
              setValue(newImages.length !== 0 ? (newImages[0] as any) : null)
            },
            onImageClick: () => {
              if (value != null)
                Taro.previewImage({
                  current: value.url,
                  urls: []
                })
            }
          }))}
        />
      </CustomInput>
      <View className="user-form__actions">
        <AtButton
          className="user-form__action-button"
          type="primary"
          disabled={formDisabled}
          onClick={() => handleSubmit()}
        >
          {!formDisabled ? '提交信息' : true ? '提交中...' : '正在加载...'}
        </AtButton>
      </View>
      <AtModal isOpened={confirmFlag} onClose={modalCancel}>
        <AtModalHeader>确认关键信息</AtModalHeader>
        <AtModalContent>
          <View className="confirm-modal">
            <View className="confirm-modal__msg">您修改了关键信息：</View>
            <View className="confirm-modal__main-msg">
              {modalMsg.map(msg => {
                const [label, value] = msg
                return (
                  <View className="confirm-modal__msg" key={label}>
                    <View className="confirm-modal__msg-label">{label}</View>：
                    <View className="confirm-modal__msg-value">{value}</View>
                  </View>
                )
              })}
            </View>
            <View className="confirm-modal__plain-msg">
              这些信息一经提交，
              <Text className="critical-msg">无法修改</Text>
              ，请谨慎操作！如需修改，请通过
              <Text className="critical-msg">联系客服</Text>联系管理员
            </View>
          </View>
        </AtModalContent>
        <AtModalAction>
          <Button onClick={modalConfirm}>确认并提交</Button>
          <Button onClick={modalCancel}>返回修改</Button>
        </AtModalAction>
      </AtModal>
    </View>
  )
}

UserFormPage.config = {
  navigationBarTitleText: '填写个人信息'
}

export default UserFormPage
