import Taro, {
  FC,
  usePullDownRefresh,
  useEffect,
  useDidShow,
  useShareAppMessage,
  useMemo
} from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Text } from '@tarojs/components'
import { AtSearchBar, AtActivityIndicator } from 'taro-ui'
import { GoodCard } from '@/components'

import { Good, getGoodsAPI, GoodStatus } from '@/models'
import useSWR from 'swr'

import { scale, encodeParams } from '@/utils'

import './index.scss'

import { mockImgSet } from '@/assets/mock/img'

const HomePage: FC = () => {
  const { data: allGoods, error: goodErr, revalidate: fetchGood } = useSWR<
    Good[]
  >(getGoodsAPI())
  const goods = useMemo(
    () =>
      allGoods == null
        ? allGoods
        : allGoods.filter(g => g.status === GoodStatus.ForSell),
    [allGoods]
  )

  // revalidate data after re-entering home page
  useDidShow(() => {
    fetchGood()
  })

  // pull down refresh feature
  usePullDownRefresh(() => {
    Taro.showNavigationBarLoading()
    fetchGood().then(() => {
      Taro.hideNavigationBarLoading()
      Taro.stopPullDownRefresh()
    })
  })

  // show error toast when errors occur
  useEffect(() => {
    if (goodErr != null) {
      console.error(goodErr)
      Taro.showToast({
        title: `加载失败`,
        icon: 'none',
        duration: 2000
      })
    }
  }, [goodErr])

  // share config
  useShareAppMessage(() => {
    return {
      title: '蛋壳',
      path: '/pages/home/index'
    }
  })

  return (
    <View className="index-page">
      <View
        className="sub-tips"
        onClick={() => Taro.switchTab({ url: '/pages/stars/index' })}
      >
        <View className="sub-tips__content">
          {/* 不想错过物品上新？想抓住每一个捡漏机会？快去添加订阅便可及时收到物品上新提醒啦 */}
          不想错过物品上新？想抓住每一个捡漏机会？多多收藏心仪的物品以便持续关注哦
        </View>
        <View className="sub-tips__icon at-icon item-extra__icon-arrow at-icon-chevron-right"></View>
      </View>
      <View className="page-content">
        <View
          className="pseudo-search-bar"
          // onClick={() => Taro.redirectTo({ url: '/pages/search/index' })}
          onClick={() => Taro.navigateTo({ url: '/pages/search/index' })}
        >
          <AtSearchBar
            actionName="搜索"
            value={''}
            onChange={() => {}}
            disabled
          />
        </View>
        <Swiper indicatorDots autoplay circular interval={4500} duration={500}>
          {scale(mockImgSet, 3).map(src => (
            <SwiperItem key={src} className="banner">
              <Image className="banner__img" src={src} mode="aspectFit" />
            </SwiperItem>
          ))}
        </Swiper>
        <View className="item-list">
          <View className="item-list__title">猜你喜欢</View>
          {!!goods &&
            goods.map(good => (
              <View
                key={good.id}
                className="item-list__item"
                onClick={() =>
                  Taro.navigateTo({
                    url: `/pages/goodDetail/index${encodeParams({
                      id: good.id
                    })}`
                  })
                }
              >
                <GoodCard good={good} />
              </View>
            ))}
          {!goods && !goodErr && (
            <View className="item-list__loading">
              <AtActivityIndicator />
              <Text className="item-list__loading-text">加载中...</Text>
            </View>
          )}
        </View>
      </View>
    </View>
  )
}

HomePage.config = {
  navigationBarTitleText: '蛋壳',
  enablePullDownRefresh: true
}

export default HomePage
