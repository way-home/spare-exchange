import Taro, { FC, useState, useMemo } from '@tarojs/taro'
import { View, Image, Button } from '@tarojs/components'
import { AtSegmentedControl } from 'taro-ui'
import { OrderCard } from '@/components'

import { Good, Order, OrderStatus, getOrdersAPI } from '@/models'

import useSWR from 'swr'
import { encodeParams, useStabilizer } from '@/utils'

import { TabType } from './params'

import './index.scss'
import { useUserStore } from '@/store'

import homeIcon from '@/assets/img/tabbar/home.png'
import sellIcon from '@/assets/img/tabbar/sell.png'

const gotoGoodPage = (id: Good['id']) =>
  Taro.navigateTo({
    url: `/pages/goodDetail/index${encodeParams({ id })}`
  })

const OngoingOrders: FC = () => {
  const [current, setCurrent] = useState(TabType.BoughtList)
  const [{ id: me }] = useUserStore()
  const { data: orders } = useSWR<Order[]>(getOrdersAPI())

  const successOrders = useMemo(
    () => orders && orders.filter(o => o.status === OrderStatus.Success),
    [orders]
  )
  const boughtList = useMemo(
    () => successOrders && successOrders.filter(o => o.buyer === me),
    [successOrders]
  )
  const soldList = useMemo(
    () => successOrders && successOrders.filter(o => o.seller === me),
    [successOrders]
  )

  const isBoughtList = current === TabType.BoughtList
  const list = isBoughtList ? boughtList : soldList

  const homeStabilizer = useStabilizer(300)

  // TODO: optional, add pulling down to refresh feature in this page
  return (
    <View className="historical-orders-page">
      <View className="historical-orders-page__tab-bar-wrapper">
        <View className="historical-orders-page__tab-bar">
          <AtSegmentedControl
            values={['我买的', '我卖的']}
            current={current}
            onClick={c => setCurrent(c)}
          />
        </View>
      </View>
      <View className="historical-orders-page__orders-list">
        {list &&
          list.map(order => (
            <View
              key={order.id}
              className="historical-orders-page__orders-list-item"
              onClick={() => gotoGoodPage(order.good)}
            >
              <OrderCard order={order}>
                <View
                  className="actions-container"
                  onClick={e => e.stopPropagation()}
                >
                  {/* work around */}
                  {/* {isBoughtList && (
                    <Button
                      className="action-widget"
                      onClick={() => {
                        console.log('投诉') // TODO: finish the logic here
                      }}
                    >
                      投诉
                    </Button>
                  )}
                  <Button
                    className="action-widget"
                    onClick={() => {
                      console.log('评价') // TODO: finish the logic here
                    }}
                  >
                    评价
                  </Button> */}
                </View>
              </OrderCard>
            </View>
          ))}
        {list != null &&
          (current === TabType.BoughtList ? (
            <View className="no-more-orders">
              似乎没有更多订单了，去
              <View
                className="no-more-orders__action"
                onClick={homeStabilizer(() => {
                  Taro.switchTab({ url: '/pages/home/index' })
                })}
              >
                <Image className="no-more-orders__icon" src={homeIcon} />
                首页
              </View>
              逛逛，看看有没有什么心仪的商品吧！
            </View>
          ) : (
            <View className="no-more-orders">
              似乎没有更多订单了，您可以
              <View
                className="no-more-orders__action"
                onClick={homeStabilizer(() => {
                  Taro.switchTab({ url: '/pages/sell/index' })
                })}
              >
                <Image className="no-more-orders__icon" src={sellIcon} />
                上传新的物品
              </View>
            </View>
          ))}
      </View>
    </View>
  )
}

OngoingOrders.config = {
  navigationBarTitleText: '已成交订单管理'
}

export default OngoingOrders
