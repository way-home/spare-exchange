import Taro, { FC } from '@tarojs/taro'
import { View, Button } from '@tarojs/components'
import { AtList, AtListItem } from 'taro-ui'
import { UserCard, Modal } from '@/components'
import { useModalState, useShowModal } from '@/components/modal/useModalState'
import { loginModalConfig } from '@/components/authModal'

import { useUserStore, LoginStatus } from '@/store'

import { gotoWebpage } from '@/utils'

import './index.scss'

const UserPage: FC = () => {
  const [user, userActions] = useUserStore()

  const authorized = user.loginStatus === LoginStatus.Authorized
  const pending = user.loginStatus === LoginStatus.Pending
  const notAuthorized = user.loginStatus === LoginStatus.Failed

  const [modalState, modalActions] = useModalState()
  const showModal = useShowModal(modalActions)

  const showLoginModal = () =>
    new Promise(resolve =>
      showModal({
        ...loginModalConfig(userActions, {
          onSuccess: () => resolve(true),
          onFailed: () => resolve(false)
        }),
        onEnter: () => Taro.hideTabBar({}),
        onLeave: () => Taro.showTabBar({})
      })
    )

  return (
    <View className="user-page">
      <View className="user-page__container">
        <View className="user-page__user-card">
          <UserCard
            user={user}
            size="large"
            detailLayout="vertical"
            contentLevel={authorized ? 'minimal' : 'empty'}
            onClick={() => {
              if (authorized)
                Taro.navigateTo({
                  url: `/pages/userHome/index?id=${user.id}`
                })
            }}
          >
            {pending && <View className="login-info">加载中...</View>}
            {notAuthorized && (
              <View
                className="login-info"
                onClick={e => {
                  e.stopPropagation()
                  showLoginModal()
                }}
              >
                点击登录
              </View>
            )}
            {/* {authorized && (
              <View className="user-card-actions">
                <Button
                  className="user-card-action"
                  onClick={e => {
                    e.stopPropagation()
                    console.log('我的店铺二维码') // TODO: generate or fetch QR code
                  }}
                >
                  我的店铺二维码
                </Button>
              </View>
            )} */}
            {/* work around */}
            {authorized && (
              <View className="user-card-actions">
                <Button
                  className="user-card-action"
                  style="width: 100px;padding: 5px 20px 27px"
                  onClick={e => {
                    console.log('我的小店')
                  }}
                >
                  我的小店
                </Button>
              </View>
            )}
          </UserCard>
        </View>
        <AtList>
          <AtListItem
            title="我的信息"
            onClick={async () => {
              if (notAuthorized && !(await showLoginModal())) return
              Taro.navigateTo({ url: '/pages/userInfo/index' })
            }}
          />
          {/* TODO: to be implemented */}
          {/* <AtListItem title="我的订阅" /> */}
          <AtListItem
            title="物品管理"
            onClick={async () => {
              if (notAuthorized && !(await showLoginModal())) return
              Taro.navigateTo({ url: '/pages/myGoods/index' })
            }}
          />
          <AtListItem
            title="待确认订单"
            onClick={async () => {
              if (notAuthorized && !(await showLoginModal())) return
              Taro.navigateTo({ url: '/pages/ongoingOrders/index' })
            }}
          />
          <AtListItem
            title="已成交订单"
            onClick={async () => {
              if (notAuthorized && !(await showLoginModal())) return
              Taro.navigateTo({ url: '/pages/historicalOrders/index' })
            }}
          />
          {/* TODO: to be implemented */}
          {/* <AtListItem title="关注服务号以及时接收物品动态消息" /> */}
          {/* TODO: to be implemented */}
          {/* <AtListItem title="帮助中心" /> */}
          <AtListItem
            title="关于我们"
            onClick={() =>
              gotoWebpage({
                url: 'https://mp.weixin.qq.com/s/wEVb3v27bdX8aIu-LJyt1w'
              })
            }
          />
        </AtList>
      </View>
      <Modal state={modalState} />
    </View>
  )
}

UserPage.config = {
  navigationBarTitleText: '我的信息'
}

export default UserPage
