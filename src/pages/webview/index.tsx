import Taro, { FC, useEffect } from '@tarojs/taro'
import { View, WebView } from '@tarojs/components'
import './index.scss'

const WebPage: FC = () => {
  const { params = {} } = Taro.useRouter()
  const url = decodeURIComponent(params['url'] || '')
  const title = decodeURIComponent(params['title'] || '')

  useEffect(() => {
    Taro.setNavigationBarTitle({ title })
  }, [title])

  return (
    <View className="webView">
      <WebView src={url} />
    </View>
  )
}

WebPage.config = {
  navigationBarTitleText: ''
}

export default WebPage
