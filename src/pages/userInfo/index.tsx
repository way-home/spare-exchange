import Taro, { FC, useEffect, useRouter } from '@tarojs/taro'
import { View, Block, Image } from '@tarojs/components'
import { AtAvatar, AtList, AtListItem } from 'taro-ui'
import { AtListItemProps } from 'taro-ui/types/list'
import './index.scss'

import { useUserStore, LoginStatus } from '@/store/user'

import { getUserInfoAPI, BasicUser, defaultBasicUser } from '@/models'
import useSWR from 'swr'

import classNames from 'classnames'

import { useStabilizer, parseUrl } from '@/utils'

type Mode = 'contact' | 'all' | 'self'

const UserInfoPage: FC = ({}) => {
  const { params } = useRouter()
  const id = params.id != null ? parseInt(params.id) : null
  const mode: Mode = (params.mode as Mode) || id != null ? 'contact' : 'self'

  const [{ loginStatus, ...me }] = useUserStore()
  const authorized = loginStatus === LoginStatus.Authorized

  const { data: userInfo } = useSWR<BasicUser>(
    authorized && id != null ? getUserInfoAPI(id) : null
  )
  useEffect(() => {
    if (userInfo != null) {
      Taro.setNavigationBarTitle({
        title: `用户${userInfo.wx_info.nickName}的${
          mode === 'contact' ? '联系方式' : '个人信息'
        }`
      })
    }
  }, [userInfo])

  const user: BasicUser | void =
    (mode === 'self' ? me : userInfo) || defaultBasicUser

  const {
    wx_info: { avatarUrl, nickName, wx_num, wx_image },
    user_info: { number, name, college, email, phone }
  } = user

  const actionStabilizer = useStabilizer(200)

  const gotoEdit = actionStabilizer(() => {
    Taro.navigateTo({ url: '/pages/userForm/index' })
  })

  interface PropsConfig {
    check?: boolean
    copy?: boolean
  }

  const itemProps = (
    data: string | null | undefined,
    {
      check = false, // check if prop exists. if it does, disable editing
      copy = false // enable touch to copy
    }: PropsConfig = {}
  ): Partial<AtListItemProps> => {
    if (mode === 'self') {
      return {
        note: data != null ? data : '',
        ...(check && data != null
          ? { arrow: undefined, extraText: undefined, onClick: undefined }
          : {
              arrow: 'right',
              extraText: '修改',
              onClick: gotoEdit
            })
      }
    } else {
      return {
        note: data != null ? data : '',
        ...(data != null && copy
          ? {
              extraText: '复制',
              onClick: actionStabilizer(() => {
                Taro.setClipboardData({ data })
              })
            }
          : { extraText: undefined, onClick: undefined })
      }
    }
  }

  const wxQrCodeCls = classNames('wx-qr-code', {
    'wx-qr-code--empty': wx_image == null
  })

  return (
    <View className="user-info-page">
      <View className="at-row at-row__justify--between at-row__align--center user-avatar">
        <View className="at-col">
          {mode === 'self' ? '我的头像' : '用户头像'}
        </View>
        <View className="at-col at-col-1 at-col--auto">
          <AtAvatar
            size="large"
            circle
            image={avatarUrl ? parseUrl(avatarUrl) : nickName ? nickName : ''}
          />
        </View>
      </View>
      <AtList hasBorder={false}>
        <AtListItem title="昵称" note={nickName ? nickName : ''} />
        {mode !== 'contact' && (
          <Block>
            <AtListItem title="姓名" {...itemProps(name, { check: true })} />
            <AtListItem title="学号" {...itemProps(number, { check: true })} />
            <AtListItem title="学院" {...itemProps(college, { check: true })} />
          </Block>
        )}
        <AtListItem title="常用邮箱" {...itemProps(email, { copy: true })} />
        <AtListItem title="电话" {...itemProps(phone, { copy: true })} />
        <AtListItem title="微信号" {...itemProps(wx_num, { copy: true })} />
      </AtList>
      <View
        className={wxQrCodeCls}
        onClick={
          mode === 'self'
            ? gotoEdit
            : () => {
                if (wx_image != null)
                  Taro.previewImage({ current: wx_image, urls: [wx_image] })
              }
        }
      >
        <View className="wx-qr-code__top-tip">
          <View className="wx-qr-code__title">微信二维码</View>
          {mode === 'self' ? (
            <View className="wx-qr-code__edit">
              修改
              <View className="at-icon at-icon-chevron-right wx-qr-code__edit-icon"></View>
            </View>
          ) : (
            <View className="wx-qr-code__note">
              {wx_image != null ? '点击图片后长按保存' : ''}
            </View>
          )}
        </View>
        <View className="wx-qr-code__image-container">
          {wx_image != null && (
            <Image
              className="wx-qr-code__image"
              mode="aspectFit"
              src={wx_image}
            />
          )}
        </View>
      </View>
    </View>
  )
}

UserInfoPage.config = {
  navigationBarTitleText: '个人信息'
}

export default UserInfoPage
