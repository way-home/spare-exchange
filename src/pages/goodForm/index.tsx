import Taro, { FC, useState, useEffect, useMemo, useRouter } from '@tarojs/taro'
import { View, Picker, Text, Block } from '@tarojs/components'
import {
  AtInput,
  AtTextarea,
  AtImagePicker,
  AtButton,
  AtMessage
} from 'taro-ui'
import { CustomInput } from '@/components'

import {
  Category,
  createGood,
  Good,
  Image,
  getCategoriesAPI,
  getGoodAPI,
  getGoodsAPI,
  updateGood,
  GoodStatus,
  createImage,
  queryISBN
} from '@/models'

import useSWR, { mutate, trigger } from 'swr'
import {
  useForm,
  fallbackAdapter,
  GenericComponentAdapter as GCA,
  yupSchema,
  num2money,
  priceRegex,
  PartialNullable,
  diffObject,
  omit,
  echo,
  fromEntries,
  exactlyPick,
  pick
} from '@/utils'
import {
  string as yupString,
  object as yupObject,
  array as yupArray,
  number as yupNumber,
  InferType
} from 'yup'

import './index.scss'

// work around for the bad design of type declaration of taro-ui
// import { ComponentClass } from 'react'

// type ComponentClassProps<Comp> = Comp extends ComponentClass<infer T>
//   ? T
//   : never
// type AtImagePickerProps = ComponentClassProps<typeof AtImagePicker>
// type File = AtImagePickerProps['files'][0]

const priceTester: [string, (v: string) => boolean] = [
  '您输入的价格有误',
  v => {
    if (v === '') return true
    const p = parseFloat(v)
    return p !== NaN && p > 0
  }
]

const validateSchema = yupObject({
  category: yupString().required('商品类别不能空缺'),
  name: yupString()
    .required('商品名称不能空缺')
    .max(30, '商品名称不能超过30字'),
  current_price: yupString()
    .required('商品价格不能空缺')
    .max(20, '您确定您的商品要出如此高价吗？')
    .matches(priceRegex, {
      message: '您输入的价格有误，小数点后数字请少于两位',
      excludeEmptyString: true
    })
    .test('current_price', ...priceTester),
  original_price: yupString()
    .max(20, '您确定您的商品有这么高的原价吗？')
    .matches(priceRegex, {
      message: '您输入的价格有误，小数点后数字请少于两位',
      excludeEmptyString: true
    })
    .test('original_price', ...priceTester),
  description: yupString()
    .required('商品描述不能空缺')
    .max(150, '您输入的描述太长了'),
  images: yupArray()
    .of(
      yupObject({
        url: yupString().matches(/\.(png|jpg|jpeg|gif)$/, '不支持的图片格式'),
        file: yupObject({
          path: yupString(),
          size: yupNumber()
        }).notRequired()
      })
    )
    .max(3, '最多只能上传3张图片'),
  author: yupString(),
  isbn: yupString().matches(/^(\d{10}|\d{13})$/, {
    message: 'ISBN码有误，不是一个10位或13位的数字串',
    excludeEmptyString: true
  }),
  press: yupString()
})

type FormType = InferType<typeof validateSchema>

const initFormData: FormType = {
  category: '',
  name: '',
  current_price: '',
  original_price: '',
  description: '',
  images: [],
  author: '',
  isbn: '',
  press: ''
}

const good2Form = (good: Good): Partial<FormType> => {
  const { category, images, ...rest } = good
  const formVal: Partial<FormType> = { ...initFormData }
  let k, v
  for ([k, v] of Object.entries(
    exactlyPick(rest, ...Object.keys(initFormData))
  )) {
    if (v != null && v !== '') formVal[k] = v
  }
  formVal.category = category.description
  if (images && images.length > 0)
    formVal.images = images.map(img => ({ url: img.image, file: undefined }))
  return formVal
}

const commonAdapter: GCA<keyof FormType, FormType> = configs => {
  return {
    ...fallbackAdapter(configs),
    error: configs.error != null
  }
}

const customInputAdapter: GCA<keyof FormType, FormType> = ({
  formDisabled,
  error
}) => {
  return {
    disabled: formDisabled,
    error: error != null
  }
}

const sendErrMsg = (message: string) => {
  if (message !== '')
    Taro.atMessage({
      message,
      type: 'error',
      duration: 1500
    })
}

type UploadMode = 'upload' | 'upload-scan' | 'upload-from'

type EditMode = 'edit' | 'edit-available'

type FormMode = UploadMode | EditMode

const isEditMode = (mode: FormMode): mode is EditMode =>
  mode.match(/^edit(-|$)/) != null

const GoodFormPage: FC = () => {
  const { params } = useRouter()
  const mode: FormMode = (params.mode as FormMode) || 'upload'
  const id = params.id != null ? parseInt(params.id) : null

  const { data: good } = useSWR<Good>(id != null ? getGoodAPI(id) : null)
  const origForm = useMemo(() => (good != null ? good2Form(good) : null), [
    good
  ])
  const imageMapping = useMemo(
    () =>
      good != null
        ? fromEntries(good.images.map(img => [img.image, img.id]))
        : {},
    [good]
  )

  const { data: categories } = useSWR<Category[]>(getCategoriesAPI())
  const categoryList = useMemo(
    () => categories && categories.map(c => c.description),
    [categories]
  )

  const {
    register,
    handleSubmit,
    formDisabled,
    values,
    setValues,
    setValueAndValidate,
    validate,
    setFormDisabled
  } = useForm<FormType>({
    initialData: initFormData,
    onError: errs => sendErrMsg(Object.values(errs)[0] as string),
    validateSchema: yupSchema(validateSchema),
    defaultAdapter: commonAdapter,
    onSubmit: async ({ values, setErrors, onError }) => {
      const errHandler = (err: any) => {
        console.error(err)
        if (err.response != null) {
          const { data, status } = err.response
          if (status === 400 && data != null) {
            // notify backend error
            setErrors(data)
            onError(data)
          } else {
            sendErrMsg(`发生错误：${status}`)
          }
        } else {
          sendErrMsg(`发生错误：${err.message}`)
        }
      }

      if (isEditMode(mode)) {
        if (id == null) {
          console.error('good id is not available')
          return
        }
        if (origForm == null) {
          console.error('good data is not available')
          return
        }
        let diff = diffObject(
          omit(origForm, 'images'),
          omit(values, 'images'),
          null,
          v => v == null || v === ''
        )
        const { category, ...restDiff } = diff
        let payload: PartialNullable<Good> = { ...restDiff }
        if (category != undefined)
          payload.category =
            category !== null
              ? categories!.find(c => c.description === category)!
              : category
        if (mode === 'edit-available') {
          payload.status = GoodStatus.ForSell
        }

        // upload images
        let newImages: Image[] = []
        let img
        for (img of values.images) {
          const { url } = img
          const id = imageMapping[url]
          try {
            newImages.push(
              id != null ? { id, image: '' } : await createImage(url)
            )
          } catch (err) {
            errHandler(err)
            return // upload image failed, abort
          }
        }
        payload.images = newImages

        // update good
        try {
          mutate(getGoodAPI(id), await updateGood({ ...payload, id: id }))
          trigger(getGoodsAPI())
        } catch (err) {
          errHandler(err)
          return // update good failed, abort
        }

        // ui feedback
        Taro.navigateBack()
      } else {
        // upload mode:
        const { category, images, ...rest } = values
        let payload: Partial<Good> = {}
        payload.category = categories!.find(c => c.description === category)!
        let k, v
        for ([k, v] of Object.entries(rest)) {
          if (v !== '') payload[k] = v
        }
        if (mode === 'upload-from') {
          payload.status = GoodStatus.ForSell
        }

        // upload images
        let newImages: Image[] = []
        let img
        for (img of images) {
          const { url } = img
          const id = imageMapping[url]
          try {
            newImages.push(
              id != null ? { id, image: '' } : await createImage(url)
            )
          } catch (err) {
            errHandler(err)
            return // upload failed, abort
          }
        }
        payload.images = newImages

        // create good
        try {
          await createGood(payload)
        } catch (err) {
          errHandler(err)
          return // create good failed, abort
        }
        trigger(getGoodsAPI())

        // ui feedback
        if (mode === 'upload-from') {
          Taro.navigateBack()
        } else {
          Taro.showToast({
            title: '成功创建',
            icon: 'success',
            duration: 1500
          })
        }
      }
    }
  })
  const category = values.category

  useEffect(() => {
    if (
      categories == null ||
      (mode === 'edit' && good == null) ||
      mode === 'upload-scan'
    ) {
      setFormDisabled(true)
      Taro.showNavigationBarLoading()
    } else {
      setFormDisabled(false)
      Taro.hideNavigationBarLoading()
    }
  }, [categories, mode, good])

  useEffect(() => {
    // form auto fill for editing
    if (origForm != null) {
      setValues(val => ({
        ...val,
        ...origForm
      }))
    }
  }, [origForm, setValues])

  useEffect(() => {
    if (mode === 'upload-scan') {
      ;(async () => {
        const { result: isbn } = await Taro.scanCode({
          scanType: ['barCode']
        })
        const {
          publisher: press,
          author,
          price: original_price,
          title: name
        } = await queryISBN(isbn)
        setValues(val => ({
          ...val,
          category: '图书',
          name,
          original_price,
          author,
          isbn,
          press
        }))
      })().finally(() => {
        setFormDisabled(false)
        Taro.hideNavigationBarLoading()
      })
    }
  }, [mode, setValues])

  return (
    <View className="good-form">
      <AtMessage />
      <CustomInput
        {...register('category', customInputAdapter)}
        required
        label="类别"
      >
        {categoryList && categories ? (
          <Picker
            onCancel={() => validate('category').catch(() => {})}
            mode="selector"
            value={category ? categoryList.findIndex(n => n === category) : 0}
            onChange={e =>
              setValueAndValidate('category', categoryList[e.detail.value])
            }
            range={categoryList}
          >
            {category ? (
              <Text>{category}</Text>
            ) : (
              <Text style="color:#808080">请选择类别</Text>
            )}
          </Picker>
        ) : (
          <Text style="color:#808080">请选择类别</Text>
        )}
      </CustomInput>
      <AtInput
        {...register('name')}
        required
        title="商品名"
        type="text"
        placeholder="最多30字"
        maxLength={30}
      />
      <AtInput
        {...register('current_price')}
        required
        title="价格(元)"
        type="digit"
        placeholder="请输入商品价格"
      />
      <AtInput
        {...register('original_price')}
        title="原价(元)"
        type="digit"
        placeholder="请输入商品原价"
      />
      {category && category === '图书' && (
        <Block>
          <AtInput
            {...register('author')}
            title="作者"
            type="text"
            placeholder="请输入图书作者"
          />
          <AtInput
            {...register('isbn')}
            title="ISBN"
            type="number"
            placeholder="请输入图书ISBN"
          />
          <AtInput
            {...register('press')}
            title="出版社"
            type="text"
            placeholder="请输入图书出版社"
          />
        </Block>
      )}
      <CustomInput
        {...register('description', customInputAdapter)}
        required
        label="描述"
        labelPosition="top"
        labelStyle="width:154rpx; padding-top:18rpx;"
        iconPosition="top"
        iconStyle="padding-top:30rpx;"
      >
        <AtTextarea
          {...register('description', configs => ({
            ...commonAdapter(configs),
            onChange: e => configs.setValue(e.detail.value)
          }))}
          maxLength={150}
          height={200}
          placeholder="请输入品牌型号、新旧程度、入手渠道、转手原因等详细描述"
        />
      </CustomInput>
      <CustomInput
        label="图片"
        labelPosition="top"
        labelStyle="width:134rpx; padding-top:10rpx"
        iconPosition="top"
        iconStyle="padding-top:30rpx"
        {...register('images', customInputAdapter)}
      >
        <AtImagePicker
          multiple
          showAddBtn
          count={3}
          onFail={e => console.log(e)}
          {...register(
            'images',
            ({ formDisabled, value, setValueAndValidate }) => ({
              showAddBtn: !(formDisabled || value.length >= 3),
              files: value,
              onChange: newImages => {
                setValueAndValidate(newImages as any)
              },
              onImageClick: i =>
                Taro.previewImage({
                  current: value[i].url,
                  urls: []
                })
            })
          )}
        />
      </CustomInput>
      <View className="good-form__actions">
        <AtButton
          className="good-form__action-button"
          type="primary"
          disabled={formDisabled}
          onClick={() => handleSubmit()}
        >
          {!formDisabled
            ? '提交物品'
            : categories != null
            ? '提交中...'
            : '正在加载...'}
        </AtButton>
      </View>
    </View>
  )
}

GoodFormPage.config = {
  navigationBarTitleText: '填写物品信息'
}

export default GoodFormPage
